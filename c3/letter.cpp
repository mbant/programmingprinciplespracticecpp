#include "../std_lib_facilities.h"

int main()
{
    cout << "Please enter the first name of the person you want to write to and another friend's name\n";
    string first_name{"???"};
    string friend_name{"???"};
    cin >> first_name >> friend_name;

    cout << "Enter f if " << friend_name << " is female, m otherwise\n";
    char friend_sex{0};
    cin >> friend_sex;

    cout << "Please enter " << first_name << "'s age\n";
    int age{-1};
    cin >> age;

    if( age <=0 )
        simple_error("You're kidding!");
    
    cout << "Dear " << first_name << " ,\n";
    cout << "How are you? I am fine. I miss you.\n";
    cout << "Have you seen " << friend_name << " recently? ";
    
    if( friend_sex == 'f' )
    {
        cout << "I miss her too.\n";
    }
    else if( friend_sex == 'm' )
    {
        cout << "I miss him too.\n";
    }

}
