#include "../std_lib_facilities.h"

int main()
{

    int n_squares {0};
    double current_grains {0};
    double previous_grains {0};

    cout << "We want to go up to how many squares .. ?\n";
    int required_squares{0};
    cin >> required_squares;

    // first square
    ++n_squares;
    current_grains = 1;

    cout << "We are currently on the " << n_squares << "th square, with "<<current_grains<<
        " grains of rice, and we have cumulated a total of " << previous_grains+current_grains << " grains.\n";
        //ugly but will do, we'd need to check the last digit for 1,2,3 to get the "st" "nd" "rd" right...

    // other squares
    while( n_squares < required_squares )
    {
        ++n_squares;
        previous_grains += current_grains;
        current_grains *= 2;

        cout << "We are currently on the " << n_squares << "th square, with "<<current_grains<<
            " grains of rice, and we have cumulated a total of " << previous_grains+current_grains << " grains.\n";
    }

    // overflow with ints on the 32nd square
    // "overflow" to inf on the 1024 square, exact number way lower than that

    return 0;
}