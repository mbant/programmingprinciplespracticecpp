#include "../std_lib_facilities.h"

int main()
{

    // consider the first prime to be 2
    vector<int> primes = {2};
    bool prime {true};

    // from 3 on (to 100), check if a number is prime
    for(int num=3; num <= 100; ++num)
    {
        prime = true;

        for( int p : primes )
        {
            prime = prime && ( num % p != 0 );
        }

        if( prime )
            primes.push_back(num);

    }

    cout << "\nPrimes found:\n";
    for( int p : primes )
        cout << p << " ";
    cout << '\n';

    return 0;
}