#include "../std_lib_facilities.h"

int main()
{

    // consider the first prime to be 2
    vector<int> primes = {2};
    bool prime {true};

    cout << "Please input the maximum number up to which you want to look up primes\n";
    int max{2};
    cin >> max;

    // from 3 on (to 100), check if a number is prime
    int num{3};
    while( num<max )
    {
        prime = true;

        for( int p : primes )
        {
            prime = prime && ( num % p != 0 );
        }

        if( prime )
            primes.push_back(num);

        ++num;
    }

    cout << "\nPrimes found:\n1 "; // BAD DESIGN, I WANT TO PRINT 1 but it won't be in primes for computations...
        // insert(1,0) is a waste though, right? all that shifting..
    for( int p : primes )
        cout << p << " ";
    cout << '\n';

    return 0;
}