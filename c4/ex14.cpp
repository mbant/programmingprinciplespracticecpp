#include "../std_lib_facilities.h"

int main()
{

    // consider the first prime to be 2
    vector<int> primes = {};

    cout << "Please input the maximum number up to which you want to look up primes\n";
    int max{2};
    cin >> max;

    // the minus 2s from here on are because the vector's corresponding integers start at 2 rather than on 0

    // create a full-false vector
    // vector<bool> composite(max-2,false); // sadly vector bool is weird 
    // https://stackoverflow.com/questions/17794569/why-is-vectorbool-not-a-stl-container
    vector<int> composite(max-2,0);

    int p {2};
    // only p needs to stay here, the other variables are initialised INSIDE THE LOOP!
    // https://stackoverflow.com/questions/7959573/declaring-variables-inside-loops-good-practice-or-bad-practice

    while( p*p < max )
    {

        // enumerate the multiples of p and tag them
        int factor {p}; // optimisation
        while( p*factor < max )
        {
            composite[p*factor - 2] = 1;   
            ++factor;
        }

        // find the next non-composite number
        int next{p-2+1}; 

        while( (next < max-2) )
        {
            if( composite[next] == 1 )
                ++next;
            else
                break;
        }

        p = next+2;

    }

    primes.push_back(1);
    for( int i {0}; i<composite.size() ; ++i )
        if( composite[i] == 0 )
            primes.push_back(i+2);

    cout << "\nPrimes found:\n";
    for( int pr : primes )
        cout << pr << " ";
    cout << '\n';


    return 0;
}

/*
for max = 150000

[marco@localhost c4]$ g++ ex14.cpp
[marco@localhost c4]$ time ./a.out
real    0m0.021s
user    0m0.018s
sys     0m0.003s
[marco@localhost c4]$ g++ ex12.cpp
[marco@localhost c4]$ time ./a.out
real    0m8.877s
user    0m8.855s
sys     0m0.001s


talking about speed-ups...
(ex14 though needs to store a vector of O(max) elements)
*/