#include "../std_lib_facilities.h"

/*
Write out the map between lowercase characters and integers
*/

int main()
{

    char iterator{'a'};

    while( iterator <= 'z' )  // this is weird but works because of internal conversions/conventions.
    {

        cout << iterator << '\t' << int{iterator} << '\n';

        ++iterator;
    }

    return 0;
}