#include "../std_lib_facilities.h"

int main()
{

    cout << "Please insert a sequence of distance between cities on a route (numbers) and press Ctrl+D to stop\n";

    double num{0.0};
    vector<double> distances = {};

    double smallest{0},largest{0},median{0},total_distance{0};

    while( cin>>num )
    {
        distances.push_back(num);
        // total_distance += num; // let's do this as the exercise wants
    }

    // sort the distances
    sort(distances);

    // find the sum
    for( auto x : distances )
        total_distance += x;
    
    //find the smallest
    smallest = distances[0];
    
    //find the largest
    largest = distances[distances.size()-1];

    // find the median
    if( distances.size() % 2 != 0 ) // odd length
        median = distances[(distances.size()-1)/2];
    else
        median = 0.5 * ( distances[distances.size()/2-1] + distances[distances.size()/2] );

    cout << total_distance << " " << smallest << " " << largest << " " << median << "\n";

    return 0;
}