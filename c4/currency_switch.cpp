#include "../std_lib_facilities.h"

/*
This programs perform currency conversions between €, $ and £.
The conversion rates are guesses.
The compiler would warns us about the use of the currency symbols are character constants, 
    as they are not basic characters (ASCII) but more complex objects [hence we won't use them]
*/
int main()
{
    int amount{0};
    char currency{0};

    cout << "Please insert an amount followed by its currency, coded as p for british pounds, e for euros, d for US dollars\n";
    cin >> amount >> currency;

    constexpr double e_to_d = 1.35;  // note we use constexpr as these won't change at runtime
    constexpr double p_to_d = 1.5;  // and are known at compile time.

    
    switch (currency)
    {
        case 'e': case 'E':
            cout << amount << "€ = " << amount * e_to_d << "$ = " << amount * e_to_d / p_to_d << "£.\n";
            break;
    
        case 'd': case 'D':
            cout << amount << "$ = " << amount / e_to_d << "€ = " << amount / p_to_d << "£.\n";
            break;

        case 'p': case 'P':
            cout << amount << "£ = " << amount * p_to_d / e_to_d << "€ = " << amount * p_to_d << "$.\n";
            break;

        default:
            cout << "I don't know any currency '"<<currency<<"' ! Sorry ... \n";
            break;
    }

    return 0;
}