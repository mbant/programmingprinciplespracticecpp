#include "../std_lib_facilities.h"

/*
Write out the map between lowercase characters and integers
*/

int main()
{

    for( char iterator{'a'} ; iterator <= 'z' ; ++iterator )  // this is weird but works because of internal conversions/conventions.
        cout << iterator << '\t' << int{iterator} << '\n';

    return 0;
}