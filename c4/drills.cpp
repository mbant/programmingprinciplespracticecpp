#include "../std_lib_facilities.h"

int main()
{

    double num1{0},num2{0};
    cout << "Please enter two numbers\n";
    while( cin >> num1 >> num2 )
    {
        cout << "You entered " << num1 << " and " << num2 << "\n";
        
        // OLD -- less efficient, more clear but not sure if I should implement this way
        // cout << "The smaller value is: " << min(num1,num2) << "\n";
        // cout << "The larger value is: " << max(num1,num2) << "\n";

        // old, more reusedcode below
        // if( num1 > num2 )
        // {
        //     cout << "The smaller value is: " << num2 << "\n";
        //     cout << "The larger value is: " << num1 << "\n";

        //     if( (num1 - num2) < 0.01 )
        //         cout << "They're almost equals though!\n";
        // }else if( num2 > num1 )
        // {
        //     cout << "The smaller value is: " << num1 << "\n";
        //     cout << "The larger value is: " << num2 << "\n";

        //     if( (num2 - num1) < 0.01 )
        //         cout << "They're almost equals though!\n";
        // }else
        // {
        //     cout << "The two numbers are equal!";            
        // }

        if( num1 == num2 )
        {
            cout << "The two numbers are equal!";            
        }else 
        {
            if( num1 > num2 )        
            {
                cout << "The smaller value is: " << num2 << "\n";
                cout << "The larger value is: " << num1 << "\n";
            }else // if( num2 > num1 )
            {
                cout << "The smaller value is: " << num1 << "\n";
                cout << "The larger value is: " << num2 << "\n";
            }

        if( abs(num2 - num1) < 0.01 )
            cout << "They're almost equals though!\n";
        }

        cout << "\nPlease enter two more numbers or Ctrl+D to stop\n";
    }

    cout << "Bye!\n";

    return 0;
}