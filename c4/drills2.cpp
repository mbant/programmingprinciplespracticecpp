#include "../std_lib_facilities.h"

double convert_to_m(double value, string unit)
{
    constexpr double m_to_cm = 100;
    constexpr double in_to_cm = 2.54;
    constexpr double ft_to_in = 12;

    if( unit != "m" )
    {
        if( unit == "cm" )
            value /= m_to_cm;
        else if ( unit == "in" )
            value = value*in_to_cm/m_to_cm;
        else // only remaining case is ft
            value = value*ft_to_in*in_to_cm/m_to_cm;
    }
    return value;
}

int main()
{

    double smallest{0},largest{0},num{0},sum{0};
    string unit{""};
    vector<double> values_log = {};

    cout << "Please enter a number and a measurement unit (in cm, m, in, ft)\n";
    cin >> num >> unit;
    
    cout << "\nYou entered " << num << unit << "\n";
    num = convert_to_m(num,unit);
    smallest = largest = sum = num;
    values_log.push_back(num);

    cout << "\nPlease enter one more number and a measurement unit (in cm, m, in, ft) or Ctrl+D to stop\n";
    while( cin >> num >> unit )
    {
        cout << "\nYou entered " << num << unit << "\n";

        if( unit != "cm" && unit != "m" && unit != "in" && unit != "ft" )
        {
            cout << "Sorry I don't know any '"<<unit<<"' unit .. \n";
            return 1;
        }

        num = convert_to_m(num,unit);

        if( smallest > num )
        {
            smallest = num;
            cout << "It's the smallest so far!\n";
        }else if( largest < num )
        {
            largest = num;
            cout << "It's the largest so far!\n";
        }

        sum += num;
        cout << "The total sum so far is " << sum << "meters\n";
       
        cout << "\nPlease enter one more number and a measurement unit (in cm, m, in, ft) or Ctrl+D to stop\n";

        values_log.push_back(num);
    }

    cout << "You've entered the following values in meters .. \n";
    for( auto v : values_log )
        cout << v << " ";

    cout << "\nSorted from the smallest to the largest this is .. \n";
    sort(values_log); // this sorts in place
    for( auto v : values_log )
        cout << v << " ";

    cout << "\nBye!\n";

    return 0;
}