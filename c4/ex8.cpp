#include "../std_lib_facilities.h"

int main()
{

    int n_squares {0};
    int current_grains {0};
    int previous_grains {0};

    cout << "Tell me up to how many grains you want to go\n";
    int required_total{0};
    cin >> required_total;

    // first square
    ++n_squares;
    current_grains = 1;

    cout << "We are currently on the " << n_squares << "th square, with "<<current_grains<<
        " grains of rice, and we have cumulated a total of " << previous_grains+current_grains << " grains.\n";
        //ugly but will do, we'd need to check the last digit for 1,2,3 to get the "st" "nd" "rd" right...

    // other squares
    while( (previous_grains+current_grains) < required_total )
    {
        ++n_squares;
        previous_grains += current_grains;
        current_grains *= 2;

        cout << "We are currently on the " << n_squares << "th square, with "<<current_grains<<
            " grains of rice, and we have cumulated a total of " << previous_grains+current_grains << " grains.\n";
    }

    return 0;
}