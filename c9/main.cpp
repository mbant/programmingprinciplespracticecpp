#include "Rational.h"

int main()
{

    Rational r1{ 1,3 };
    Rational r2{ 3,4 };

    cout << r1+r2 << '\n';
    cout << r1-r2 << '\n';
    cout << r1*r2 << '\n';
    cout << r1/r2 << '\n';

    return 0;
}