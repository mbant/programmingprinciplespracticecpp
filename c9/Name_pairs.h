#include "../std_lib_facilities.h"

/* 
Symbolic constants 

these just  make the code cleaner, so them being global is no big deal I think
*/

constexpr char numeric = '8';
constexpr char quit = 'q';
constexpr char stop = ';';
const char name_string = 'N'; //name token, arbitrary kind 'N'



class Token
{
    /*
    Object to store the tokens.
    kind:
        'q',';',
        (constexpr numeric) for numeric types
        (constexpr name) for string types
    value:
        if kind == numeric then value is initialised as the numeric value
        if kind == name then name is initialised as a string value
    */
    public:
        char kind; // defines which kind of token
        double value{0}; // only double values for numerical Tokens
        string name{""};

        Token(char ch)    // make a Token from a char
        :kind(ch), value(0) { }    
        Token(char ch, double val)     // make a Token from a char and a double
        :kind(ch), value(val) { }
        Token(char ch, string name_)     // make a Token from a char and a string
        :kind(ch), name(name_) { }
};

class Token_stream 
{
    public:
        Token_stream();   // make a Token_stream that reads from cin
        Token_stream(istream& stream);   // make a Token_stream that reads from cin
        Token get();
        void putback(Token t);
        void ignore(char c); // ignore tokens untill we read one like c
            // c should be just a character, cause we don't want to transform input into Tokens here
            // not only is wasteful but also risky

    private:
        istream& input_stream;
        bool full{false}; // indicator for full buffer
        Token buffer; // no need to implement complex dynamic buffers, only one space possible
};



class Name_Pairs
{
    public:
        void read_names();
        void read_ages();

        string print() const;
        void sort();

    private:
        vector<size_t> sort_indexes();

        vector<string> names;
        vector<double> ages;

        vector<size_t> idx;
        Token_stream ts;
};

ostream& operator<<(ostream& os, const Name_Pairs& np);