#include "Name_pairs.h"

// The constructor just sets full to indicate that the buffer is empty:
Token_stream::Token_stream(istream& input)
:full(false), buffer(0),input_stream(input)    // no Token in buffer
{
}

Token_stream::Token_stream()
:Token_stream(cin)    // no Token in buffer
{
}

Token Token_stream::get()
{
    if( full )
    {
        full=false;
        return buffer;
    }else
    {
        // read a character from input_stream
        char c{' '};
        input_stream >> c;

        if( input_stream ) // if not reached EOF signal
        {
            switch (c)
            {
                case quit: case stop:
                    return Token(c);

                case '.': 
                case '1': case '2': case '3': case '4': case '5': 
                case '6': case '7': case '8': case '9': case '0': // numeric tokens, if it starts with '.' or a digit it's a number
                // potential problem for input like .a
                {
                    input_stream.putback(c); // putback to the input stream the first character
                    //read the value
                    double val{0};
                    input_stream >> val;
                    return Token(numeric,val);
                }
                default:
                    if ( isalpha(c) )    // “Is c a letter?”
                    {
                        string s;
                        s += c;
                        while ( input_stream.get(c) && ( isalpha(c) || isdigit(c) ) ) s+=c; // read all the alphanumeric characters
                        input_stream.putback(c);
                        return Token(name_string,s); // else is a (syntactically valid) variable name
                    }
                    error("Bad token ( "+string{c}+" ) inserted, not sure what you meant.");
            }

        }else{
            return Token(quit); // EOF reached, return quit
        }
    }
}

void Token_stream::putback( Token t )
{
    if( !full )
    {
        full = true;
        buffer = t;
    }else
        error("Token_stream buffer already full, only one putback allowed!");
}


void Token_stream::ignore(char c)
{
    // first look in buffer:
    if (full && c==buffer.kind) {
        full = false;
        return;
    }
    
    full = false;
    
    // now search input:
    char ch = 0;
    while (input_stream>>ch)
        if (ch==c) 
            return;
}


void Name_Pairs::read_names()
{
    cout << "Insert a series of names and "<< stop <<" to stop:\n";

    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case numeric:
            {
                // "Expected a name, not a number!";
                error("Expected a name, not a number.");
            }
            case name_string:
            {
                names.push_back( t.name );
                t = ts.get(); // get the next token and go on
                break;
            }
            case stop:
            {
                // ok, end of list
                // initialize original index locations
                idx = vector<size_t>(names.size());
                iota(idx.begin(), idx.end(), 0);
                return;
            }
            default:
                // no defined Token, return error
                error("Bad token inserted, not sure what you meant.");
        }
    }
}

void Name_Pairs::read_ages()
{
    unsigned int names_number = names.size();
    unsigned int ages_number = ages.size();

    if ( names_number == ages_number )
    {
        cout << "There's a age for every name, insert more names first!\n";
        return;
    }   
    else if ( names_number < ages_number )
        error("We have too many ages!");

    cout << "Insert an age for eah name you've inserted (and "<< stop <<" to stop):\n";
    Token t = ts.get();    

    while(true)
    {

        switch ( t.kind )
        {
            case numeric:

                ages.push_back(t.value);
                ++ages_number;
                t = ts.get();
                if( ages_number > names_number )
                {
                    cout << "Thanks, got enough ages...\n";
                    return;
                }
                break;

            case name_string:
            {
                error("Expected a number, not a name.");
            }
            case stop:
            {
                // ok, end of list
                return;
            }
            default:
                // no defined Token, return error
                error("Bad token inserted, not sure what you meant.");
        }
    }
}

string Name_Pairs::print() const
{

    string repr;
    
    for( unsigned int i=0; i<names.size(); ++i)
    {
        if( i<ages.size() )
        {
            repr += "(" + names[idx[i]] + "," + std::to_string(ages[idx[i]]) + ")\n";
        }
    }

    return repr;
}



vector<size_t> Name_Pairs::sort_indexes()
{
  // initialize original index locations
  idx = vector<size_t>(names.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(),
       [this](size_t i1, size_t i2) {return this->names[i1] < this->names[i2];});

  return idx;
}


void Name_Pairs::sort()
{   
    // fake sort, internally calls sort_indexes, print uses that to print
    if ( names.size() > 0 )
        sort_indexes();
}


// overall I think this class is an ass, why would I implement it as vectors of names and vectors of ages and not a singe vector of pairs?
// I could build the class for the pairs, no problem, but this just sucks....
// and the operator overloadings requested are weird with the structure like this! while they would make sense for a single pair


ostream& operator<<(ostream& os, const Name_Pairs& np)
{
    return os << np.print();
}