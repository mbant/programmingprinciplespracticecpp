// dangerously ugly code
struct X {
    void f(int x)
    {
        struct Y {
            int f() { return 1; } 
            int m; 
        };
        int m;
        m=x;
        Y m2;
        return f( m2.f() ); 
    }

    int m; 
    
    void g(int m) 
    {
        if (m) 
            f(m+2); 
        else 
        {
            g(m+2); 
        }
    }

    X() { } 
    void m3() { }

};

int main() // needed to switch to return int
{
    X a; 
    a.f(2);
}

// can you even put a main in a struct?! let's take it out

// anyway it just goes to segfault because it recursively calls itself without returning anything, so you get an infinite call stack