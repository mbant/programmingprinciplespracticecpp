#include "../std_lib_facilities.h"

class Rational
{
    public:
        
        Rational();

        Rational(long int numerator, long int denominator):
            numerator_(numerator),denominator_(denominator)
            {
                if( !is_valid() )
                    throw Division_by_zero{};
                simplify();
            }

        inline long int numerator() const { return numerator_; }
        inline long int denominator() const { return denominator_; }
        
        // how do Imake this privates? Do I have to declare the helper functions as 'friends' ?
        static vector<long int> primes;     // this might be a good exercise to test dynamic programming
        static long int max_prime_computed;

        class Division_by_zero{};

    private:
        long int numerator_, denominator_;
        bool is_valid() const ;
        void simplify();

};

vector<long int> Rational::primes{1,2,3,5,7}; // init with a few...
long int Rational::max_prime_computed = 7;

bool Rational::is_valid() const 
{ 
    if( denominator_ == 0 ) 
        return false;
    return true;
};

Rational zero_rational()
{
    static Rational zero { 0 , 1 };
    return zero;
}

Rational::Rational():
            numerator_(zero_rational().numerator()),
            denominator_(zero_rational().denominator())
            {  }

void compute_primes_less_than(long int max) // this will compute all primes less than max, store it into Rational::primes
    // supposedly I need to call this only from is_prime()
{
    // first, check wether max is already less than the maximum prime computed
    long int previous_max = Rational::max_prime_computed;
    Rational::max_prime_computed = max-1;

    if ( max < previous_max )
        return;


    // else we need to check all the numbers from previous_max+1 to max
    long int current_start = previous_max+1;
    vector<int> composite(max-current_start,0);
    // of course this means we always start from the last prime + 1, eg. 7+1, while we might have already checked that 8 and 9 are no primes
    // and those searches would be deleted

    auto current_old_prime = std::begin(Rational::primes) +1 ; // this is 2
    long int p {*current_old_prime}; // we still start sieving for multiples of all primes, starting from 2
    while( p*p < max )
    {
        // char tmp; cin>>tmp;

        // enumerate the multiples of p and tag them
        long int factor { std::max( p , (long int)std::ceil((double)current_start/p) ) }; // optimisation -- current_start/p is integer division, but I need it to round up!
        // should I worry about narrowing conversions in (long int)std::ceil((double)current_start/p) ?
        while( p*factor < max )
        {
            composite[p*factor - current_start] = 1;   
            ++factor;
        }


        // find the next non-composite number:
        if ( (current_old_prime+1) != std::end(Rational::primes) ) // if we're still below the previously found primes
        {
            ++current_old_prime;
            p = *current_old_prime;
        }
        else    // else we're looking for the ones in the 'composite' vector
        {
            long int next { p - previous_max + 1 }; // recall composite always starts from the max number we've called this with + 1 (so the first time we're here p=previous_max-1)
                                                // note that every number from Rational::primes.back() and Rational::max_prime_computed is composite, so it won't matter if I skip those
            
            while( (next < (max-current_start)) )
            {
                if( composite[next] == 1 )
                    ++next;
                else
                    break;
            }

            p = current_start + next;            
        }        
    }

    for( long int i {0}; i<composite.size() ; ++i )
        if( composite[i] == 0 )
            Rational::primes.push_back(i+current_start);

    return;
}

bool is_prime(long int a)
{
    if( a < 0 ) a = - a; // only positive numbers accepted, primes make sense only for positives, the negatives are just a reflection

    if ( a > Rational::max_prime_computed )
        compute_primes_less_than(a+1);

    return std::binary_search( std::begin(Rational::primes), std::end(Rational::primes), a) ;
}

// Deprecated ?
std::pair<vector<long int>::iterator,vector<long int>::iterator> get_indexes_primes_less_than(long int max)
{
    return std::pair<vector<long int>::iterator,vector<long int>::iterator>
        { 
            std::begin(Rational::primes) , 
            std::lower_bound( std::begin(Rational::primes) , std::end(Rational::primes) , max )
        };
}

vector<long int> prime_factors(long int a)
{
    vector<long int> factors;

    is_prime(a); // compute the necessary primes
    
    auto current_prime = Rational::primes.begin()+1; // we don't want to use 1, so get the second!

    long int res{a};
    while( !is_prime(res) )  // this shouldn't invalidate the pointer as res <= a
    {
        if ( res % (*current_prime) == 0 )
        {
            factors.push_back(*current_prime);
            res /= *current_prime;
        }
        else
        {
            std::advance(current_prime,1); // ++current_prime; // std::advance(current_prime,1) , just slightly more good-looking
        }
    }
    factors.push_back(res);
    return factors;
}

long int greatest_common_factor_unsorted(long int a, long int b)
{

    vector<long int> a_factors = prime_factors(a);
    vector<long int> b_factors = prime_factors(b);

    vector<long int> common_factors;

    for ( auto const& fa : a_factors )                  // this doens't take advantage of the fact that the two vectors are sorted
    {
        auto fb_iterator = std::begin(b_factors);

        while ( fb_iterator != std::end(b_factors) ) 
        {
            if( *fb_iterator == fa )
            {
                common_factors.push_back(fa);
                b_factors.erase(fb_iterator);
                break;
            }
            ++fb_iterator;
        }
    }

    long int gcf = 1;
    for ( auto const& n : common_factors )
        gcf *= n;

    return gcf;
}


long int greatest_common_factor(long int a, long int b) // this takes advantage of the fact that the two vectors are sorted
{

    vector<long int> a_factors = prime_factors(a);
    vector<long int> b_factors = prime_factors(b);

    vector<long int> common_factors;

    auto fa_iterator = std::begin(a_factors);
    auto fb_iterator = std::begin(b_factors);

    while ( fa_iterator != std::end(a_factors) && fb_iterator != std::end(b_factors) )
    {
        if ( *fa_iterator < *fb_iterator )
            ++fa_iterator;
        else if ( *fa_iterator > *fb_iterator )
            ++fb_iterator;
        else // they're equal!
        {
            common_factors.push_back(*fa_iterator);
            ++ fa_iterator;
            ++fb_iterator;
        }
    }

    long int gcf = 1;
    for ( auto const& n : common_factors )
        gcf *= n;

    return gcf;
}

void Rational::simplify( )
{
    if( numerator_ == 0 ) // checks for the denominator happen in is_valid()
    {
        denominator_ = 1;
    }
    else
    {
        auto gcf = greatest_common_factor(numerator_,denominator_);
        numerator_ /= gcf;
        denominator_ /= gcf;
    }
}

long int least_common_multiple(long int a, long int b)
{

    vector<long int> a_factors = prime_factors(a);
    vector<long int> b_factors = prime_factors(b);

    // count the number of occurrences of each prime factor
    // (the first call to operator[] initialized the counter with zero)
    std::map<long int, long int>  prime_map;
    for (const auto &num : a_factors) {
        ++prime_map[num];
    }
 
    std::map<long int, long int>  prime_map_b;
    for (const auto &num : b_factors) {
        ++prime_map_b[num];
    }

    // merge the two maps maintaining the highest number of repetitions
    for ( const auto &e : prime_map_b )
    {
        prime_map[e.first] = std::max ( prime_map[e.first] , e.second );
    }

    
    long int lcm = 1;
    for ( const auto &e : prime_map )
    {
        lcm *= std::pow((double)e.first, e.second);
    }
    
    return lcm;

}


// **** Now onto mathematical operations

Rational operator* ( const Rational& a, const Rational& b)
{
    if( a.numerator() == 0 || b.numerator() == 0 ) // the check for the denominator is done inside the constructor
        return zero_rational();
    return Rational{ a.numerator() * b.numerator() , a.denominator() * b.denominator() };
}

Rational operator/ ( const Rational& a, const Rational& b)
{
    if( a.numerator() == 0 || b.denominator() == 0 ) // the check for the denominator is done inside the constructor (would be weird if b.den() is zero though..)
        return zero_rational();
    return Rational{ a.numerator() * b.denominator() , a.denominator() * b.numerator() }; // multiply by its reciprocal
}

Rational operator+ ( const Rational& a, const Rational& b)
{
    long int lcm = least_common_multiple(a.denominator(),b.denominator());
    long int new_numerator = ( a.numerator() * lcm / a.denominator() ) + ( b.numerator() * lcm / b.denominator() );
    if( new_numerator == 0 )
        return zero_rational();
    return Rational{ new_numerator , lcm };
}

Rational operator- ( const Rational& a, const Rational& b)
{
    long int lcm = least_common_multiple(a.denominator(),b.denominator());
    long int new_numerator = ( a.numerator() * lcm / a.denominator() ) - ( b.numerator() * lcm / b.denominator() );
    if( new_numerator == 0 )
    {
        return zero_rational();
    }
    return Rational{ new_numerator , lcm };
}

// final helper 

ostream& operator<<(ostream& os, Rational r)
{
    return os << r.numerator() << "/" << r.denominator();

}