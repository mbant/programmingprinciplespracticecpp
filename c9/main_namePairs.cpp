#include "Name_pairs.h"

int main()
{
    try
    {

        Name_Pairs n;

        n.read_names();
        n.read_ages();

        cout << n;
        n.sort();
        cout << "Sorted:\n";
        cout << n;
        
    }
    catch (const std::exception& e)
    {
        cerr << "Error: " << e.what() << "\n";
    }

    return 0;
}