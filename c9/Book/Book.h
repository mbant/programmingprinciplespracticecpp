#include "../../std_lib_facilities.h"
# include "../Chrono.h"

#ifndef BOOK
#define BOOK 1234567


enum class Genre
{
    fiction = 1, nonfiction, periodical, biography, children
};

class Book
{
    public:

        class Invalid_ISBN { };
        class Invalid_check_out { };
        class Invalid_check_in { };

        Book (const string& title, const string& author, const string& ISBN, Genre genre, const Chrono::Date& copyright_date, bool checked_out):
            title_(title), author_(author), ISBN_(ISBN), genre_(genre), copyright_date_(copyright_date),checked_out_(checked_out)
        {
                if (!is_ISBN(ISBN)) throw Invalid_ISBN{};
        }

        Book (const string& title, const string& author, const string& ISBN ):
            title_(title), author_(author), ISBN_(ISBN)
        {
                if (!is_ISBN(ISBN)) throw Invalid_ISBN{};
        }

        Book();

        inline string title () const { return title_; }
        inline string author () const { return author_; }
        inline string ISBN () const { return ISBN_; }
        inline const Chrono::Date& copyright_date () const { return copyright_date_; } // return a constant reference, for viewing purposes only
        inline Genre genre () const { return genre_; }
        inline bool checked_out () const { return checked_out_; }

        void check_out();
        void check_in();

    private:
        string title_;
        string author_;
        string ISBN_;

        Genre genre_;
        Chrono::Date copyright_date_;
        bool checked_out_{false};

        bool is_ISBN(const string& ISBN); // true is ISBN is valid (format checking)

};

bool operator==(const Book& a, const Book& b);
bool operator!=(const Book& a, const Book& b);
ostream& operator<<(ostream& os, const Book& a);

#endif