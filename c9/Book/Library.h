#include "../../std_lib_facilities.h"
#include "../Chrono.h"
#include "Book.h"
#include "Patron.h"

#ifndef LIBRARY
#define LIBRARY 123456789

struct Transaction
{
    Transaction(Book book, Patron patron, Chrono::Date date):
        book_(book), patron_(patron), date_(date){ }

    Book book_;
    Patron patron_;
    Chrono::Date date_;
};

class Library
{
    public:
        Library(){}

        void add_book( const Book& b ){ books_.push_back(b); }
        void add_patron( const string user_name ){ patrons_.push_back ( Patron{user_name,library_number_counter} ); ++library_number_counter; }
        // note we pass just a user name to the library, so it can manage the card numbers itself
        
        void check_out( Book& b, Patron& p, Chrono::Date& today ); // I'd love to get a default value of today = Date{ std::chrono::system_clock::now() }; but not worth it
        void check_out_slow( Book& b, Patron& p, Chrono::Date& today );

        inline const vector<Book>& books() const { return books_; }
        inline const vector<Patron>& patrons() const { return patrons_; }
        inline const vector<Transaction>& transactions() const { return transactions_; }

        vector<Patron> get_Portugueses() const;

        bool has_book(const Book& b) const;
        bool has_book_available(const Book& b) const;
        void check_out_book(const Book& b);
        bool has_patron(const Patron& p) const;
        bool does_patron_owe_fees(const Patron& p) const;
        void add_fee_to_patron(const Patron& p);

        class No_Such_Book{};
        class No_Such_Patron{};
        class Fees_Owed{};
        class Doubled_patron{};

    private:

        unsigned long library_number_counter{0};

        vector<Book> books_;
        vector<Patron> patrons_;
        vector<Transaction> transactions_;

};
#endif