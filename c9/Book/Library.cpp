#include "Library.h"

bool operator==(const Patron& a, const Patron& b )
{
    return ( a.library_card_number() == b.library_card_number() );
}

ostream& operator<<( ostream& os, const Patron& p )
{
    os << p.user_name();
    if( p.owes_fee() )
        os << "( who owes " << p.owed_fee() << ")";
    return os << '\n';
}


bool Library::has_book(const Book& b) const
{
    for( auto& book : books_ )
        if ( book == b )
            return true;
    return false;
}

bool Library::has_book_available(const Book& b) const
// here assume at least a copy of the book has been found in the library already, but is there one available?
{
    for( auto& book : books_ )
        if ( book == b && !( book.checked_out() ) )
            return true;
    return false;
}

void Library::check_out_book(const Book& b)
{
    for( auto& book : books_ )
        if ( book == b && !( book.checked_out() ) )
        {
            book.check_out();
            return;
        }
    throw No_Such_Book{};
}

bool Library::has_patron(const Patron& p) const
{
    for( auto& patron : patrons_ )
        if ( patron == p )
            return true;
    return false;
}

bool Library::does_patron_owe_fees(const Patron& p) const
{
    for( auto& patron : patrons_ )
        if ( patron == p )
            return patron.owes_fee() ;

    throw No_Such_Patron{};
}

void Library::add_fee_to_patron(const Patron& p)
{
    for( auto& patron : patrons_ )
        if ( patron == p )
        {
            patron.add_fee(1);
            return;
        }

    throw No_Such_Patron{};
}

void Library::check_out_slow( Book& b, Patron& p , Chrono::Date& today )
{
    if ( ! has_book(b) )
        throw No_Such_Book{};
    if ( ! has_patron(p) )
        throw No_Such_Patron{};
    if ( ! has_book_available(b) )
        throw Book::Invalid_check_out{};
    if ( does_patron_owe_fees(p) )
        throw Fees_Owed{};


    // ok, so check the book out and write the transaction
    check_out_book(b);
    add_fee_to_patron(p);
    transactions_.push_back ( Transaction{b,p,today} );
    return;
}

// what bothers me of all this code set-up is that we need to loop through each book and patron multiple times.
// if made all in one function we'd avoid it, so let's write down that version even if premature optimimsation is bad...
void Library::check_out( Book& b, Patron& p , Chrono::Date& today )
{
    for( auto& patron : patrons_ )
    {
        if ( patron == p && !( patron.owes_fee() ) )
        {
            for( auto& book : books_ )
            {
                if ( book == b && !( book.checked_out() ) )
                {
                    book.check_out();
                    patron.add_fee(1);
                    transactions_.push_back ( Transaction{b,p,today} );
                    return;
                }
            }

            if ( ! has_book(b) )
                throw No_Such_Book{};
            else
                throw Book::Invalid_check_out{};
        }
    }

    if ( ! has_patron(p) )
        throw No_Such_Patron{};
    else
        throw Fees_Owed{};

}
// This should be better and only loops more than once if we need to throw exceptions

// so I could return a vector of (smart) pointers to portugueses, but that sorf of complicate everything else.
// let me just return a copy vector. A vector of references won't do because the vector p gets deleted right after return.
// even going around that I might delete a patron at some point and I'd be left with an invalid pointer inside the vector, so smart pointers are needed...
// still just an exercise
vector<Patron> Library::get_Portugueses() const // lol, sorry, https://it.wikipedia.org/wiki/Fare_il_portoghese
{
    vector<Patron> p{0};
    for( auto& patron : patrons_ )
        if( patron.owes_fee() )
            p.push_back ( patron );

    return p;
}