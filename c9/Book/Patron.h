#include "../../std_lib_facilities.h"

#ifndef PATRON
#define PATRON 12345678


class Patron
{
    public:

        Patron( const string& user_name, const unsigned long library_card_number ):
            user_name_(user_name), library_card_number_(library_card_number) { }

        Patron(): Patron("A Patron has no name",0){}

        inline void add_fee( double f ){ owed_fee_ += f; }
        inline void pay_fee( double f ){ owed_fee_ = std::max(0.,owed_fee_-f); } // every excess payed is a donation to the library! :)

        inline string user_name() const { return user_name_; }
        inline unsigned long library_card_number() const { return library_card_number_; }
        inline double owed_fee() const { return owed_fee_; }
        inline bool owes_fee() const { return (owed_fee_ > 0.) ; }

    private:
        string user_name_;
        unsigned long library_card_number_;
        double owed_fee_{0};
};

bool operator==(const Patron& a, const Patron& b );
ostream& operator<<( ostream& os, const Patron& p );


#endif