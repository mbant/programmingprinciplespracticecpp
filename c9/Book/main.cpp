#include "../../std_lib_facilities.h"
#include "../Chrono.h"
#include "Book.h"
#include "Patron.h"
#include "Library.h"

int main()
{
    Book b;
    try{
        b = Book{"Prova","Me","11-12-13-a"};
    }
    catch(...)
    {
        std::cerr << "Wrong book 1\n";
    }
    
    try{
        b = Book{"Prova","Me","11-12-a-a"};
    }
    catch(...)
    {
        std::cerr << "Wrong book 2\n";
    }

    try{
        b = Book{"Prova","Me","11-12-13-!"};
    }
    catch(...)
    {
        std::cerr << "Wrong book 3\n";
    }

    try{
        b = Book{"Prova","Me","11-13-a"};
    }
    catch(...)
    {
        std::cerr << "Wrong book 4\n";
    }

    Chrono::Date d{2019,Chrono::Month::jan,30};
    Book c{"Me","Prova","11-12-13-b",Genre::fiction,d,false};

    std::cout << b;
    std::cout << (b==c) << '\n';
    std::cout << (b!=c) << '\n';
    
    std::cout << c.copyright_date() << '\n';
    
    Patron p{"Pipo",0};
    Patron p2{"Pipa",2}; 
    // notice I could just change card number. To which library does he refer to actually??
    // yes this classes should be private to the Library maybe...
    // at least they should not be used explicitly in code outside the Library library


    Library l;
    l.add_patron(p.user_name());
    l.add_patron(p2.user_name());

    l.add_book( b );
    l.add_book( c );
    l.add_book( Book{"Me","Prova2","1-2-3-x"} );
    l.add_book( Book{"Me2","Prova","1-2-3-a"} );

    for( auto p : l.get_Portugueses() )
        cout << p.user_name() << '\n'; // should print nothing

    cout << p.owed_fee() << '\n';
    cout << c.checked_out() << '\n';

    for( auto const& b : l.books() )
        cout << b;
    cout << '\n';

    for( auto const& p : l.patrons() )
        cout << p;
    cout << '\n';


    l.check_out(b,p,d);
    // l.check_out_slow(b,p,d); // equivalent

    for( auto const& b : l.books() )
        cout << b;
    cout << '\n';

    for( auto const& p : l.patrons() )
        cout << p;
    cout << '\n';


    try
    {
        l.check_out(b,p2,d); // should throw as already checked_out
    }
    catch(...)
    {
        std::cerr << "Gotcha\n";
    }

    try
    {
        l.check_out(b,p,d); // should throw as patron owes fee
    }
    catch(...)
    {
        std::cerr << "Gotcha Again\n";
    }

    try
    {
        l.check_out(c,p2,d); // should throw as no patron with this card number! (this bothers me so much... xD)
    }
    catch(...)
    {
        std::cerr << "Gotcha Again and Again\n";
    }
    
    return 0;
}


// overall I'm not very happy about this
// we compare only using card number and ISBN but there's not many checks ...
// we woud need to match both CN and name to be sure, and we should be able to add a book given a name OR a CN (not [at leastnot only] a reference to P)
// the name would get the CN from the DB and then we'd use that to check the book out

// of course these are all tabular data, so using a proper DB with a table of patrons, a table of books and a table of transactions works best
// that'll help in ensuring multiple checks and cross-referencing books and patrons but ... 
// here is only an example/exercise!