#include "Book.h"

const Book& default_book()
{
    static Book b { string("empty"), string("empty"), string("0-0-0-0") }; // note the static!
    return b;
}

Book::Book (): //default book, so the class ca be used in vectors
    title_(default_book().title()), 
    author_(default_book().author()), 
    ISBN_(default_book().ISBN())        // no need to check ISBN as it is valid based on default_book
    {}

bool Book::is_ISBN(const string& ISBN)
/* 
Checks for valid ISBN, the grammar is
ISBN:
    'int' - 'int' - 'int' - alphanumeric character
Using std::istringstream for convenience, using the using a token stream is the best way I know to perform grammar checking
*/
{
    std::istringstream tokenStream(ISBN);
    
    unsigned int n; char c;
    for ( int i=0; i<3; ++i )
    {
        if ( !( tokenStream >> n ) )   // https://stackoverflow.com/questions/6791520/if-cin-x-why-can-you-use-that-condition
            return false;              //  note that it needs parenthesis

        tokenStream >> c;
        if ( c != '-' )
            return false;
    }
    
    tokenStream >> c;
    if ( !isalnum(c) )
        return false;
    
    if ( tokenStream >> c )  // check end of stream?
        return false;
    
    return true;
}

void Book::check_out()
{
    if ( !checked_out_ )
         checked_out_= true; 
    else
        throw Invalid_check_out{};         
}

void Book::check_in()
{
    if ( checked_out_ )
         checked_out_= false; 
    else
        throw Invalid_check_in{};         
}



bool operator==(const Book& a, const Book& b)
{
    return ( a.ISBN() == b.ISBN() );
}

bool operator!=(const Book& a, const Book& b)
{
    return !(a==b); // or return ( a.ISBN() != b.ISBN() );
}

ostream& operator<<(ostream& os, const Book& a)
{
    os << a.title() << '\t'
            << a.author() << '\t'
            << a.ISBN() << '\t';

    if( !a.checked_out() )
        os << "AVAILABLE";
    else
        os << "UN-AVAILABLE";
    
    return os << '\n';
}