// permutation and combinations .cpp

/*
Exercise text:
Design a program that asks users for two numbers, asks them whether
they want to calculate permutations or combinations, and prints out the
result. This will have several parts. Do an analysis of the above require-
ments. Write exactly what the program will have to do. Then, go into
the design phase. Write pseudo code for the program, and break it into
sub-components. This program should have error checking. Make sure
that all erroneous inputs will generate good error messages.
*/

/*
int factorial(int);
int permutation(int,int);
int combination(int,int); // defined in terms of permutation

main:
Ask for two integer numbers:
    read two ints and check types

Ask for C or P to decide which operation to perform
    switch

output the result
*/

#include "../std_lib_facilities.h"

int factorial(int n)
{
    if( n < 0 ) // not defined for negative ints
        error("Factorial not defined for negative numbers");
    else if( n <= 1 ) // for 0 and 1
        return 1;
    
    return n * factorial(n-1);
}

int permutations(int a, int b)
{
    if( b > a )
        error("In Permutation(a,b) b must be smaller than a.");
    if( b < 0 )
        error("In Permutation(a,b) , both a and b must positive.");
    return factorial(a) / factorial(a-b);
}

int combinations(int a, int b)
{
    if( b > a )
        error("In Combinations(a,b) b must be smaller than a.");
    if( b < 0 )
        error("In Combinations(a,b) , both a and b must positive.");
    return permutations(a,b) / factorial(b);
}

int main()
try
{
    cout << "Welcome to Permutations and Combinations!\n";
    cout << "Please insert two non-negative integer numbers a and b (with b<a):\n";

    double num;
    
    cin >> num;
    int a { narrow_cast<int>(num) };

    cin >> num;
    int b { narrow_cast<int>(num) };

    cout << "Now enter P if you want permutations or C if you want combinations:\n";
    char c;
    cin>>c;

    int res{0};
    
    switch (c)
    {
        case 'c': case 'C':
            res = combinations(a,b);
            cout << "Combinations(";
            break;
        case 'p': case 'P':
            res = permutations(a,b);
            cout << "Permutations(";
            break;
        default:
            error("Unknown operations / input, only P or C are accepted");
            break;
    }

    cout << a << ',' << b << ") = " << res << "\n";

    cout << "Thanks for using this program, bye!\n";
    return 0;
}
catch(exception& e)
{
    cerr << "Error: " << e.what() << '\n';
    return 1;
}
catch( ... )
{
    cerr << "Unknown error\n";
    return 2;
}