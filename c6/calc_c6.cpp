/*
calc.cpp

Tokenised calculator, with token stream to read from command 
line regular expressions and grammmar to parse them

Grammar to read the tokens:

Expression:
    Term
    Expression "+" Term
    Expression "–" Term
Term:
    Singleton
    Term "*" Singleton
    Term "/" Singleton
Singleton:                  // new definition, to deal with nary operator which bindtighter than all other binary ones
    Primary
    Primary "!"             // New feature: Factorial
Primary:
    Number
    "(" Expression ")"
    "{" Expression "}"
Number:
    floating-point-literal

*/

#include "../std_lib_facilities.h"

class Token
{
    /*
    Object to store the tokens.
    kind:
        'q','=',
        '*','/','+','-','(',')' ... operators and parenthesis are simply stored with a null value and their
            representation as kind
        '8' for numeric types
    value:
        if kind == '8' then value is initialised as the numeric value
    */
    public:
        char kind; // defines which kind of token
        double value{0}; // only double values for numerical Tokens
        Token()
        :kind('x'),value(0) { } // x is an invalid Token, used to make sure the un-initialised Token gives error
        Token(char ch)    // make a Token from a char
        :kind(ch), value(0) { }    
        Token(char ch, double val)     // make a Token from a char and a double
        :kind(ch), value(val) { }
};

class Token_stream 
{
    public:
        Token_stream();   // make a Token_stream that reads from cin
        Token get();
        void putback(Token t);

    private:
        bool full{false};
        Token buffer; // no need to implement complex dynamic buffers, only one space possible
};

// The constructor just sets full to indicate that the buffer is empty:
Token_stream::Token_stream()
:full(false), buffer()    // no Token in buffer
{
}

Token Token_stream::get()
{
    if( full )
    {
        full=false;
        return buffer;
    }else
    {
        // read a character from cin -- remember cin skips whitespace
        char c{' '};
        cin >> c;

        switch (c)
        {
            case 'q': case '=': // terminating tokens, q to quit and = to evaluate immediately
            case '*': case '/': case '+': case '-': // arithmetic tokens
            case '(': case ')': case '{': case '}': // parenthesis
            case '!':                               // factorial
                return Token(c);

            case '.': 
            case '1': case '2': case '3': case '4': case '5': 
            case '6': case '7': case '8': case '9': case '0': // numeric tokens, if it starts with '.' or a digit it's a number
            // potential problem for input like .a
            {
                cin.putback(c); // putback to the input stream the first character
                //read the value
                double val{0};
                cin >> val;
                return Token('8',val);
            }
            default:
                error("Bad token inserted, not sure what you meant.");
        }
    }
}

void Token_stream::putback( Token t )
{
    if( !full )
    {
        full = true;
        buffer = t;
    }else
        error("Token_stream buffer already full, only one putback allowed!");
}


// Define the tokenstream so that every function can use it (would need to pass it as parameter otherwise)
// or create a class Calculator which holds its Token_stream ...


Token_stream ts; // only member that needs initialisation has default value, so ok... (I think)
double expression(); // forward declaration needed for the system to work, ideally we'd have an header...


int factorial(int n)
{
    if( n < 0 ) // not defined for negative ints
        error("Factorial not defined for negative numbers");
    else if( n <= 1 ) // for 0 and 1
        return 1;
    
    return n * factorial(n-1);
}


// no need to define number ( of course? thanks istream/cin )
// but let's do it for completeness...
double number()
{ 
    Token t = ts.get();

    if( t.kind != '8' )
        error("Bad token inserted, not sure what you meant.");

    return t.value;
}

double primary()
{
    double val; // tricky, this is not initialised..
    Token t = ts.get();
    
    switch (t.kind)
    {
        case '(':
        {
            val = expression();
            t = ts.get();
            if( t.kind != ')')
                error("Missing expected closing ')' ");
            break;
        }
        case '{':
        {
            val = expression();
            t = ts.get();
            if( t.kind != '}')
                error("Missing expected closing '}' ");
            break;
        }
        default:
            ts.putback(t);
            val = number();
            break;
    }

    return val;
}

double singleton()
{
    double val = primary(); // all singletons start with a Primary
    Token t = ts.get();

    while(true)
    {
        switch (t.kind)
        {
            case '!':
                val = factorial(narrow_cast<int>(val)); // cast val back to int (might throw error) 
                                                        // and compute its factorial
                t = ts.get(); // get the next and go on
                break;
            default:
                ts.putback(t);
                return val;
        }
    }
}

double term()
{
    // get the left operator should be a singleton
    double left = singleton();
    
    // get the next token (look ahead)
    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case '*':
            {
                double right = singleton(); // read another term
                left *= right;
                t = ts.get(); // get the next and go on
                break;
            }
            case '/':
            {
                double right = singleton(); // read another term
                if( right == 0 )
                    error("Division by zero detected!");
                left /= right;
                t = ts.get(); // get the next and go on
                break;
            }
            default:
                // this is the default, only a singleton is read on left
                ts.putback(t);
                return left;
        }
    }
}

double expression()
{
    double left = term();
    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case '+':
            {
                double right = term();
                left += right;
                t = ts.get(); // get the next token and go on
                break;
            }
            case '-':
            {
                double right = term();
                left -= right;
                t = ts.get(); // get the next token and go on
                break;
            }
            default:
                // ok, no more + or -, so return
                ts.putback(t);
                return left; // finally exit
        }
    }
}

// **************************************

int main()
{

    cout << "Welcome to my calculator!\n" <<
        "Supported operations are +-*/ only\n" <<
        "Insert expressions, followed by '=' to see them evaluated\n" <<
        "Insert 'q' to quit.\n\n";

    double res {0};

    try
    {
        while( cin ) // this goes on untill the loop body breaks or Ctrl+D
        {

            Token t = ts.get();
            
            if( t.kind == 'q' )   // quit
                    break;
            else if( t.kind == '=' )   // print
            {
                cout << "\n= " << res << '\n';
                res = 0;
            }   
            else
            {
                ts.putback(t);
                res = expression();
            }
        }

    }
    catch( exception& e )
    {
        cerr << "Error: " << e.what() << '\n';
        return 1;
    }
    catch( ... )
    {
        cerr << "Unknown Error" << '\n';
        return 1;
    }

    cout << "\nThanks for using My_Calculator\nBye!\n\n";
    return 0;
}


/*
TODO:
    still can't deal with negative numbers! (as in Tokens that starts with '-' -> "-2+1=")
*/