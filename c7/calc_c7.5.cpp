/*
calc.cpp

Tokenised calculator, with token stream to read from command 
line regular expressions and grammmar to parse them

Grammar:

Calculation:
    Calculation ";" Statement
    Statement
    Print
    Help
    Quit

Statement:
    Declaration // or assignment of a variable of a constant
    Expression

Declaration:
    "let" Name "=" Expression
    "const" Name "=" Expression

Print:
    ;
Quit:
    q

Expression:
    Term
    Expression "+" Term
    Expression "–" Term

Term:
    Singleton
    Term "*" Singleton
    Term "/" Singleton
    Term "%" Singleton

Singleton:                  // new definition, to deal with unary operator which bindtighter than all other binary ones
    Primary
    Primary "!"             // New feature: Factorial

Primary:
    Number
    "sqrt" "(" Expression ")"           // added support for functions
    "pow" "(" Expression "," Expression ")"
    "(" Expression ")"
    "{" Expression "}"
    '+' Primary             // added support for negative (and specified positive) numbers
    '-' Primary

Number:
    floating-point-literal

*/

#include "../std_lib_facilities.h"

/* Symbolic constants */
constexpr char numeric = '8';
constexpr char quit = 'q';
constexpr char print = ';';
constexpr char help = 'h';

const string prompt = "> ";
const string result = "= ";

const char name = 'N'; //name token, arbitrary kind 'N'

const char let = 'L';  //let token, arbitrary kind 'L'
const char konstant = 'C';  //const token, arbitrary kind 'C'
const string declkey = "let"; // keyword used in declarations
const string constdeclkey = "const"; // keyword used in declarations
    /* ex 7.10
    changing this to # requires not only a change in declkey (most likely to a char)
    but also a change in how it's handled since now is not in the alphanumeric style anymore
    so it becomes simply a Token('#') that triggers the declaration, no need to treat it any differently
    --- summary: erase lines with ref to declkey, use constexpr let = '#' and let the rest be handled normally
    *//* ex 7.11
    this is the opposite, from a simple char Token to a keyword as
    let is in this implementation
    see also c7/calculator08buggy.cpp for example
    */    

const char sqrt_func = 'S';
const char pow_func = 'P';

class Variable
{
    public:
        string name;
        double value;
        bool constant{false};
};

class Symbol_table
{
    public:
        vector<Variable> var_table;

        double get_value(string name);
        double set_value(string name,double val);
        bool is_declared(string name);
        double define(string name,double val, bool constant);
};

double Symbol_table::get_value(string name) // get the value of the variable named 'name'
{
    for( auto v : var_table )
        if( v.name == name )
            return v.value;

    error("get: undefined variable named "+name);
}

double Symbol_table::set_value(string name, double val) // set value 'val' to Variable named 'name'
{
    for( Variable& v : var_table )
    {
        if( v.name == name )
        {
            if ( v.constant )
            {
                error(name+" was declared as constant, you can't modify its value.");
            }else{
                v.value = val;
                return val;
            }
        }
    }
    error("set: undefined variable named "+name);
}

bool Symbol_table::is_declared(string name)
{
    for( auto v : var_table )
        if( v.name == name )
            return true;
    return false;
}

double Symbol_table::define(string name, double val, bool constant=false)
{
    var_table.push_back(Variable{name,val,constant});
    return val;
}

class Token
{
    /*
    Object to store the tokens.
    kind:
        'q','=',
        '*','/','+','-','(',')' ... operators and parenthesis are simply stored with a null value and their
            representation as kind
        (constexpr numeric) for numeric types
    value:
        if kind == numeric then value is initialised as the numeric value
    */
    public:
        char kind; // defines which kind of token
        double value{0}; // only double values for numerical Tokens
        string name{""};

        Token(char ch)    // make a Token from a char
        :kind(ch), value(0) { }    
        Token(char ch, double val)     // make a Token from a char and a double
        :kind(ch), value(val) { }
        Token(char ch, string var_name)     // make a Token from a char and a string
        :kind(ch), name(var_name) { }
};

class Token_stream 
{
    public:
        Token_stream();   // make a Token_stream that reads from cin
        Token get();
        void putback(Token t);
        void ignore(char c); // ignore tokens untill we read one like c
            // c should be just a character, cause we don't want to transform input into Tokens here
            // not only is wasteful but also risky

    private:
        bool full{false};
        Token buffer; // no need to implement complex dynamic buffers, only one space possible
};

// The constructor just sets full to indicate that the buffer is empty:
Token_stream::Token_stream()
:full(false), buffer(0)    // no Token in buffer
{
}

Token Token_stream::get()
{
    if( full )
    {
        full=false;
        return buffer;
    }else
    {
        // read a character from cin -- remember cin skips whitespace
        char c{' '};
        cin >> c;

        switch (c)
        {
            case quit: case print: case help:
            case '*': case '/': case '+': case '-': case '%': // arithmetic tokens
            case '(': case ')': case '{': case '}': // parenthesis
            case '!':                               // factorial
            case '=':                               // variable declaration
            case ',':                               // function arguments separation
                return Token(c);

            case '.': 
            case '1': case '2': case '3': case '4': case '5': 
            case '6': case '7': case '8': case '9': case '0': // numeric tokens, if it starts with '.' or a digit it's a number
            // potential problem for input like .a
            {
                cin.putback(c); // putback to the input stream the first character
                //read the value
                double val{0};
                cin >> val;
                return Token(numeric,val);
            }
            default:
                if ( isalpha(c) || c == '_' )    // “Is c a letter or an underscore?”
                {
                    string s;
                    s += c;
                    while (cin.get(c) && (isalpha(c) || isdigit(c) || c == '_' )) s+=c; // read all the alphanumeric characters
                        // note that this is our definition of allowed keywords/var_names! ( similar to C++ )
                    cin.putback(c);
                    if (s == declkey) return Token(let); // declaration keyword
                    if (s == constdeclkey) return Token(konstant); // constant declaration keyword
                    if (s == "sqrt") return Token(sqrt_func); // sqrt keyword
                    if (s == "pow") return Token(pow_func); // pow keyword
                    return Token(name,s); // else is a (syntactically valid) variable name
                }
                error("Bad token ( "+string{c}+" ) inserted, not sure what you meant.");
        }
    }
}

void Token_stream::putback( Token t )
{
    if( !full )
    {
        full = true;
        buffer = t;
    }else
        error("Token_stream buffer already full, only one putback allowed!");
}


void Token_stream::ignore(char c)
{
    // first look in buffer:
    if (full && c==buffer.kind) {
        full = false;
        return;
    }
    
    full = false;
    
    // now search input:
    char ch = 0;
    while (cin>>ch)
        if (ch==c) 
            return;
}


// Define the tokenstream so that every function can use it (would need to pass it as parameter otherwise)
// or create a class Calculator which holds its Token_stream ...


Token_stream ts; // only member that needs initialisation has default value, so ok... (I think)
Symbol_table st; // to make things confusing

double expression(); // forward declaration needed for the system to work, ideally we'd have an header

int factorial(int n)
{
    if( n < 0 ) // not defined for negative ints
        error("Factorial not defined for negative numbers");
    else if( n <= 1 ) // for 0 and 1
        return 1;
    
    return n * factorial(n-1);
}

double my_pow(double base, int exponent)
{
    if( exponent == 0 )
        return 0.0;
    
    double res = base;

    for(int i=1; i<exponent; ++i)
        res *= base;
    return res;
}


// no need to define number ( of course? thanks istream/cin )
// but let's do it for completeness...
double number()
{ 
    Token t = ts.get();

    
    switch ( t.kind )
    {
        case numeric:
            return t.value;
        case name:
            return st.get_value(t.name);    
        default:
            error("Expected a number or an expression, while '"+string{t.kind}+"' was given.");
    }
}

double primary()
{
    double val; // tricky, this is not initialised..
    Token t = ts.get();
    
    switch (t.kind)
    {
        case sqrt_func:
        
            t = ts.get();
            if( t.kind != '(' )
                error("Missing opening parenthesis after function call");
            
            val = expression();
            
            t = ts.get();
            if( t.kind != ')' )
                error("Missing closing parenthesis after function call");
            
            if( val < 0 )
                error("Square root of a negative number not defined (only real numbers here)");

            val = sqrt(val);
            break;

        case pow_func:
        {    
            t = ts.get();
            if( t.kind != '(' )
                error("Missing opening parenthesis after function call");
            
            double base = expression();
            
            t = ts.get();
            if( t.kind != ',' )
                error("Missing comma, pow function needs two arguments separated by a comma");

            int exponent{0};

            try{
                exponent = narrow_cast<int>( expression() );
            }
            catch( runtime_error& e )
            {
                // throw a more customised error message
                cerr << "Error: " << e.what() << '\n';
                error("The current implementation of pow() expect an integer as second argument");
            }

            t = ts.get();
            if( t.kind != ')' )
                error("Missing closing parenthesis after function call");

            val = my_pow(base,exponent);            
            break;
        }
        case '(':
            val = expression();
            t = ts.get();
            if( t.kind != ')')
                error("Missing expected closing ')' ");
            break;

        case '{':
            val = expression();
            t = ts.get();
            if( t.kind != '}')
                error("Missing expected closing '}' ");
            break;

        case '+':
            val = primary();
            break;

        case '-':
            val = - primary(); // notice the minus
            break;

        default:
            ts.putback(t);
            val = number();
            break;
    }

    return val;
}

double singleton()
{
    double val = primary(); // all singletons start with a Primary
    Token t = ts.get();

    while(true)
    {
        switch (t.kind)
        {
            case '!':
                val = factorial(narrow_cast<int>(val)); // cast val back to int (might throw error) 
                                                        // and compute its factorial
                t = ts.get(); // get the next and go on
                break;
            default:
                ts.putback(t);
                return val;
        }
    }
}

double term()
{
    // get the left operator should be a singleton
    double left = singleton();
    
    // get the next token (look ahead)
    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case '*':
            {
                double right = singleton(); // read another term
                left *= right;
                t = ts.get(); // get the next and go on
                break;
            }
            case '/':
            {
                double right = singleton(); // read another term
                if( right == 0 )
                    error("Division by zero detected!");
                left /= right;
                t = ts.get(); // get the next and go on
                break;
            }
            case '%': // modulo operation, defined as the float version fmod() from cmath -- could also enforce int as in factorial
            {
                double right = singleton(); // read another term
                if( right == 0 )
                    error("Division by zero detected!");
                left = fmod(left,right);
                t = ts.get(); // get the next and go on
                break;
            }
            default:
                // this is the default, only a singleton is read on left
                ts.putback(t);
                return left;
        }
    }
}

double expression()
{
    double left = term();
    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case '+':
            {
                double right = term();
                left += right;
                t = ts.get(); // get the next token and go on
                break;
            }
            case '-':
            {
                double right = term();
                left -= right;
                t = ts.get(); // get the next token and go on
                break;
            }
            default:
                // ok, no more + or -, so return
                ts.putback(t);
                return left; // finally exit
        }
    }
}

double declaration( bool constant )
// assume we have seen "let” or "const"
// handle: let name = expression
// const name = expression
// declare a variable called "name” with the initial value "expression”
{
    Token t = ts.get();
    if (t.kind != name) error ("name expected in declaration");
    
    string var_name = t.name;
    
    t = ts.get();
    if (t.kind != '=') error("= missing in declaration of "+ var_name);
    
    double d = expression();
    if( st.is_declared(var_name) )
    {
        if( constant )
            error("Variable "+var_name+" already declared, can't define it as constant");
        st.set_value(var_name,d);
    }
    else{
       st.define(var_name,d,constant);
    }
    
    return d; // return the value for printing ( consistency with expression() for statement () )
}

double statement()
{
    Token t = ts.get();

    switch (t.kind)
    {
        case let:
            return declaration(false);
        case konstant:
            return declaration(true);
        default:
            ts.putback(t);
            return expression();
    }

}

// **************************************

void clean_up_mess()
{
    ts.ignore(print);
}

void calculate()
{
    while( cin ) // this goes on untill the loop body breaks or Ctrl+D
    {
        try
        {
            cout << prompt;
            Token t = ts.get();
            
            while( t.kind == print ) 
                t = ts.get(); //eat prints
            if( t.kind == quit )   // quit
                    return;
            if( t.kind == help )
                cout << "Welcome to my calculator!\n" <<
                    "Supported operations are:\n" <<
                    "\t simple arithmetics + - * / \% \n" <<
                    "\t variable declaration as let name = value\n" <<
                    "\t some function call like sqrt(float) or pow(float,integer)\n" <<
                    "Insert, possibly, one expression per line. Follow with " << print << " to see them evaluated\n" <<
                    "Insert " << quit << " to quit or "<< help << "to see this message again.\n\n";

            ts.putback(t);
            cout << result << statement() << '\n';
        }
        catch( runtime_error& e )
        {
            cerr << "Error: " << e.what() << '\n';
            clean_up_mess();
        }
    }
}

int main()
try{

    // hard-code some useful constants
    st.define("pi",3.1415926535,true);
    st.define("e",2.7182818284,true);
    st.define("k",1e3); // this can be re-assigned

    cout << "Welcome to my calculator!\n" <<
            "Supported operations are:\n" <<
            "\t simple arithmetics + - * / \% \n" <<
            "\t variable declaration as let name = value\n" <<
            "\t some function call like sqrt(float) or pow(float,integer)\n" <<
            "Insert, possibly, one expression per line. Follow with " << print << " to see them evaluated\n" <<
            "Insert " << quit << " to quit or "<< help << "to see this message again.\n\n";

    calculate();

    cout << "\nThanks for using My_Calculator\nBye!\n\n";
    return 0;

}
catch(exception& e)
{
    cerr << "Error: " << e.what() <<"\n";
    return 1;
}
catch(...)
{
    cerr << "Unknown error occurred\n";
    return 2;
}


// so cool! it works for sqrt(pow(2,1)!+2);