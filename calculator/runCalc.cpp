/*
runCalc.cpp

Tokenised calculator, with token stream to read from command 
line regular expressions and grammmar to parse them

Grammar:

Calculation:
    Calculation ";" Statement
    Statement
    Print
    Help
    Quit

Statement:
    Declaration // or assignment of a variable of a constant
    Expression

Declaration:
    "let" Name "=" Expression
    "const" Name "=" Expression

Print:
    ;
Quit:
    q

Expression:
    Term
    Expression "+" Term
    Expression "–" Term

Term:
    Singleton
    Term "*" Singleton
    Term "/" Singleton
    Term "%" Singleton

Singleton:                  // new definition, to deal with unary operator which bindtighter than all other binary ones
    Primary
    Primary "!"             // New feature: Factorial

Primary:
    Number
    "sqrt" "(" Expression ")"           // added support for functions
    "pow" "(" Expression "," Expression ")"
    "(" Expression ")"
    "{" Expression "}"
    '+' Primary             // added support for negative (and specified positive) numbers
    '-' Primary

Number:
    floating-point-literal

*/

#include "Calculator.h"

int main()
try{

    Calculator c{};

    // hard-code some useful constants
    c.st.define("pi",3.1415926535,true);
    c.st.define("e",2.7182818284,true);
    c.st.define("k",1e3,false); // this can be re-assigned

    cout << c.help_text;

    c.calculate();

    cout << c.quit_text;
    return 0;

}
catch(exception& e)
{
    cerr << "Error: " << e.what() <<"\n";
    return 1;
}
catch(...)
{
    cerr << "Unknown error occurred\n";
    return 2;
}


// so cool! it works for sqrt(pow(2,1)!+2);