#include "Calculator.h"

double Symbol_table::get_value(string name) // get the value of the variable named 'name'
{
    for( auto v : var_table )
        if( v.name == name )
            return v.value;

    error("get: undefined variable named "+name);
}

double Symbol_table::set_value(string name, double val) // set value 'val' to Variable named 'name'
{
    for( Variable& v : var_table )
    {
        if( v.name == name )
        {
            if ( v.constant )
            {
                error(name+" was declared as constant, you can't modify its value.");
            }else{
                v.value = val;
                return val;
            }
        }
    }
    error("set: undefined variable named "+name);
}

bool Symbol_table::is_declared(string name)
{
    for( auto v : var_table )
        if( v.name == name )
            return true;
    return false;
}

double Symbol_table::define(string name, double val, bool constant=false)
{
    var_table.push_back(Variable{name,val,constant});
    return val;
}

// The constructor just sets full to indicate that the buffer is empty:
Token_stream::Token_stream(istream& input)
:full(false), buffer(0),input_stream(input)    // no Token in buffer
{
}

Token_stream::Token_stream()
:Token_stream(cin)    // no Token in buffer
{
}

Token Token_stream::get()
{
    if( full )
    {
        full=false;
        return buffer;
    }else
    {
        // read a character from input_stream
        char c{' '};
        input_stream >> c;

        if( input_stream ) // if not reached EOF signal
        {
            switch (c)
            {
                case quit: case print: case help:
                case '*': case '/': case '+': case '-': case '%': // arithmetic tokens
                case '(': case ')': case '{': case '}': // parenthesis
                case '!':                               // factorial
                case '=':                               // variable declaration
                case ',':                               // function arguments separation
                    return Token(c);

                case '.': 
                case '1': case '2': case '3': case '4': case '5': 
                case '6': case '7': case '8': case '9': case '0': // numeric tokens, if it starts with '.' or a digit it's a number
                // potential problem for input like .a
                {
                    input_stream.putback(c); // putback to the input stream the first character
                    //read the value
                    double val{0};
                    input_stream >> val;
                    return Token(numeric,val);
                }
                default:
                    if ( isalpha(c) || c == '_' )    // “Is c a letter or an underscore?”
                    {
                        string s;
                        s += c;
                        while (input_stream.get(c) && (isalpha(c) || isdigit(c) || c == '_' )) s+=c; // read all the alphanumeric characters
                            // note that this is our definition of allowed keywords/var_names! ( similar to C++ )
                        input_stream.putback(c);
                        if (s == declkey) return Token(let); // declaration keyword
                        if (s == constdeclkey) return Token(konstant); // constant declaration keyword
                        if (s == "sqrt") return Token(sqrt_func); // sqrt keyword
                        if (s == "pow") return Token(pow_func); // pow keyword
                        return Token(name,s); // else is a (syntactically valid) variable name
                    }
                    error("Bad token ( "+string{c}+" ) inserted, not sure what you meant.");
            }

        }else{
            return Token(quit); // EOF reached, return quit
        }
    }
}

void Token_stream::putback( Token t )
{
    if( !full )
    {
        full = true;
        buffer = t;
    }else
        error("Token_stream buffer already full, only one putback allowed!");
}


void Token_stream::ignore(char c)
{
    // first look in buffer:
    if (full && c==buffer.kind) {
        full = false;
        return;
    }
    
    full = false;
    
    // now search input:
    char ch = 0;
    while (input_stream>>ch)
        if (ch==c) 
            return;
}


Calculator::Calculator(){ 
            help_text = "Welcome to my calculator!\n";
            help_text += "Supported operations are:\n";
            help_text += "\t simple arithmetics + - * / \% \n";
            help_text += "\t variable declaration as let name = value\n";
            help_text += "\t some function call like sqrt(float) or pow(float,integer)\n";
            help_text += "\t Insert, possibly, one expression per line. \n\t Follow with '"; 
            help_text += print;
            help_text += "' to see them evaluated\n\t Insert '";
            help_text += quit;
            help_text += "' to quit or '";
            help_text += help;
            help_text += "' to see this message again.\n\n";

            quit_text = "\nThanks for using My_Calculator\nBye!\n\n";
        }

int Calculator::factorial(int n)
{
    if( n < 0 ) // not defined for negative ints
        error("Factorial not defined for negative numbers");
    else if( n <= 1 ) // for 0 and 1
        return 1;
    
    return n * factorial(n-1);
}

double Calculator::my_pow(double base, int exponent)
{
    if( exponent == 0 )
        return 0.0;
    
    double res = base;

    for(int i=1; i<exponent; ++i)
        res *= base;
    return res;
}


// no need to define number ( of course? thanks istream/cin )
// but let's do it for completeness...
double Calculator::number()
{ 
    Token t = ts.get();

    
    switch ( t.kind )
    {
        case numeric:
            return t.value;
        case name:
            return st.get_value(t.name);    
        default:
            error("Expected a number or an expression, while '"+string{t.kind}+"' was given.");
    }
}

double Calculator::primary()
{
    double val; // tricky, this is not initialised..
    Token t = ts.get();
    
    switch (t.kind)
    {
        case sqrt_func:
        
            t = ts.get();
            if( t.kind != '(' )
                error("Missing opening parenthesis after function call");
            
            val = expression();
            
            t = ts.get();
            if( t.kind != ')' )
                error("Missing closing parenthesis after function call");
            
            if( val < 0 )
                error("Square root of a negative number not defined (only real numbers here)");

            val = sqrt(val);
            break;

        case pow_func:
        {    
            t = ts.get();
            if( t.kind != '(' )
                error("Missing opening parenthesis after function call");
            
            double base = expression();
            
            t = ts.get();
            if( t.kind != ',' )
                error("Missing comma, pow function needs two arguments separated by a comma");

            int exponent{0};

            try{
                exponent = narrow_cast<int>( expression() );
            }
            catch( runtime_error& e )
            {
                // throw a more customised error message
                cerr << "Error: " << e.what() << '\n';
                error("The current implementation of pow() expect an integer as second argument");
            }

            t = ts.get();
            if( t.kind != ')' )
                error("Missing closing parenthesis after function call");

            val = my_pow(base,exponent);            
            break;
        }
        case '(':
            val = expression();
            t = ts.get();
            if( t.kind != ')')
                error("Missing expected closing ')' ");
            break;

        case '{':
            val = expression();
            t = ts.get();
            if( t.kind != '}')
                error("Missing expected closing '}' ");
            break;

        case '+':
            val = primary();
            break;

        case '-':
            val = - primary(); // notice the minus
            break;

        default:
            ts.putback(t);
            val = number();
            break;
    }

    return val;
}

double Calculator::singleton()
{
    double val = primary(); // all singletons start with a Primary
    Token t = ts.get();

    while(true)
    {
        switch (t.kind)
        {
            case '!':
                val = factorial(narrow_cast<int>(val)); // cast val back to int (might throw error) 
                                                        // and compute its factorial
                t = ts.get(); // get the next and go on
                break;
            default:
                ts.putback(t);
                return val;
        }
    }
}

double Calculator::term()
{
    // get the left operator should be a singleton
    double left = singleton();
    
    // get the next token (look ahead)
    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case '*':
            {
                double right = singleton(); // read another term
                left *= right;
                t = ts.get(); // get the next and go on
                break;
            }
            case '/':
            {
                double right = singleton(); // read another term
                if( right == 0 )
                    error("Division by zero detected!");
                left /= right;
                t = ts.get(); // get the next and go on
                break;
            }
            case '%': // modulo operation, defined as the float version fmod() from cmath -- could also enforce int as in factorial
            {
                double right = singleton(); // read another term
                if( right == 0 )
                    error("Division by zero detected!");
                left = fmod(left,right);
                t = ts.get(); // get the next and go on
                break;
            }
            default:
                // this is the default, only a singleton is read on left
                ts.putback(t);
                return left;
        }
    }
}

double Calculator::expression()
{
    double left = term();
    Token t = ts.get();

    while(true)
    {
        switch ( t.kind )
        {
            case '+':
            {
                double right = term();
                left += right;
                t = ts.get(); // get the next token and go on
                break;
            }
            case '-':
            {
                double right = term();
                left -= right;
                t = ts.get(); // get the next token and go on
                break;
            }
            default:
                // ok, no more + or -, so return
                ts.putback(t);
                return left; // finally exit
        }
    }
}

double Calculator::declaration( bool constant )
// assume we have seen "let” or "const"
// handle: let name = expression
// const name = expression
// declare a variable called "name” with the initial value "expression”
{
    Token t = ts.get();
    if (t.kind != name) error ("name expected in declaration");
    
    string var_name = t.name;
    
    t = ts.get();
    if (t.kind != '=') error("= missing in declaration of "+ var_name);
    
    double d = expression();
    if( st.is_declared(var_name) )
    {
        if( constant )
            error("Variable "+var_name+" already declared, can't define it as constant");
        st.set_value(var_name,d);
    }
    else{
       st.define(var_name,d,constant);
    }
    
    return d; // return the value for printing ( consistency with expression() for statement () )
}

double Calculator::statement()
{
    Token t = ts.get();

    switch (t.kind)
    {
        case let:
            return declaration(false);
        case konstant:
            return declaration(true);
        default:
            ts.putback(t);
            return expression();
    }

}

// **************************************

void Calculator::clean_up_mess()
{
    ts.ignore(print);
}

void Calculator::calculate()
{
    while( true ) // this goes on until the loop body breaks -- Ctrl+D won't have effect
    {
        try
        {
            cout << prompt;
            Token t = ts.get();
            
            while( t.kind == print ) 
                t = ts.get(); //eat prints
            if( t.kind == quit )   // quit
                    return;
            if( t.kind == help )
                cout << help_text;

            ts.putback(t);
            cout << result << statement() << '\n';
        }
        catch( runtime_error& e )
        {
            cerr << "Error: " << e.what() << '\n';
            clean_up_mess();
        }
    }
}