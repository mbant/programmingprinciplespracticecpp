#include "../std_lib_facilities.h"

/* 
Symbolic constants 

these just  make the code cleaner, so them being global is no big deal I think
*/

constexpr char numeric = '8';
constexpr char quit = 'q';
constexpr char print = ';';
constexpr char help = 'h';

const string prompt = "> ";
const string result = "= ";

const char name = 'N'; //name token, arbitrary kind 'N'

const char let = 'L';  //let token, arbitrary kind 'L'
const char konstant = 'C';  //const token, arbitrary kind 'C'
const string declkey = "let"; // keyword used in declarations
const string constdeclkey = "const"; // keyword used in declarations

const char sqrt_func = 'S';
const char pow_func = 'P';

class Variable
{
    public:
        string name;
        double value;
        bool constant{false};
};

class Symbol_table
{
    public:
        vector<Variable> var_table;

        double get_value(string name);
        double set_value(string name,double val);
        bool is_declared(string name);
        double define(string name,double val, bool constant);
};

class Token
{
    /*
    Object to store the tokens.
    kind:
        'q','=',
        '*','/','+','-','(',')' ... operators and parenthesis are simply stored with a null value and their
            representation as kind
        (constexpr numeric) for numeric types
    value:
        if kind == numeric then value is initialised as the numeric value
    */
    public:
        char kind; // defines which kind of token
        double value{0}; // only double values for numerical Tokens
        string name{""};

        Token(char ch)    // make a Token from a char
        :kind(ch), value(0) { }    
        Token(char ch, double val)     // make a Token from a char and a double
        :kind(ch), value(val) { }
        Token(char ch, string var_name)     // make a Token from a char and a string
        :kind(ch), name(var_name) { }
};

class Token_stream 
{
    public:
        Token_stream();   // make a Token_stream that reads from cin
        Token_stream(istream& stream);   // make a Token_stream that reads from cin
        Token get();
        void putback(Token t);
        void ignore(char c); // ignore tokens untill we read one like c
            // c should be just a character, cause we don't want to transform input into Tokens here
            // not only is wasteful but also risky

    private:
        istream& input_stream;
        bool full{false};
        Token buffer; // no need to implement complex dynamic buffers, only one space possible
};

class Calculator
{
    public: 
        Token_stream ts; // only member that needs initialisation has default value, so ok... (I think)
        Symbol_table st; // to make things confusing

        void calculate();

        string help_text;
        string quit_text;

        Calculator();

    private:

        void clean_up_mess();
        double statement();
        double declaration( bool constant );
        double expression();
        double term();
        double singleton();
        double primary();
        double number();

        double my_pow(double base, int exponent);
        int factorial(int n);

};