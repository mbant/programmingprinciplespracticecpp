#include "../PPP-graph-lib/GUI.h"

using namespace Graph_lib;

struct Lines_window : Graph_lib::Window {
    Lines_window(Point xy, int w, int h, const string& title);
private:
    // data:
    Open_polyline lines;

    // widgets:
    Button next_button;
    Button quit_button;
    In_box next_x;
    In_box next_y;
    Out_box xy_out;
    Menu color_menu;
    Button menu_button;
    
    void change(Color c) { lines.set_color(c); }
    void hide_menu() { color_menu.hide(); menu_button.show(); }

    // actions invoked by callbacks:
    void red_pressed() { change(Color::red); hide_menu(); }
    void blue_pressed() { change(Color::blue); hide_menu(); }
    void black_pressed() { change(Color::black); hide_menu(); }
    void menu_pressed() { menu_button.hide(); color_menu.show(); }
    void next();
    void quit();
    // callback functions:
    static void cb_red(Address, Address addr) { reference_to<Lines_window>(addr).red_pressed(); }
    static void cb_blue(Address, Address addr) { reference_to<Lines_window>(addr).blue_pressed(); }
    static void cb_black(Address, Address addr) { reference_to<Lines_window>(addr).black_pressed(); }
    static void cb_menu(Address, Address addr) { reference_to<Lines_window>(addr).menu_pressed(); }
    static void cb_next(Address, Address addr) { reference_to<Lines_window>(addr).next(); }
    static void cb_quit(Address, Address addr) { reference_to<Lines_window>(addr).quit(); }
};

class Win : public Graph_lib::Window {
// four ways to make it appear that a button moves around:
// show/hide, change location, create new one, and attach/detach
public:
    Win(int w, int h, const string& t);
    ~Win();
    
    Button* p1;
    Button* p2;
    bool sh_left; // show/hide

    Button* mvp;
    bool mv_left; // move
    
    Button* cdp;
    bool cd_left; // create/destroy
    
    Button* adp1;
    Button* adp2;
    bool ad_left; // activate/deactivate

    Button* quit_button;
    int score{0};
    Text score_text;
    
    void sh(); // actions
    void mv();
    void cd();
    void ad();

    void quit();
    void increment_score();

    // callbacks to separate system code from member functions
    static void cb_sh(Address, Address addr) { reference_to<Win>(addr).sh(); } // Address is void*
    static void cb_mv(Address, Address addr) { reference_to<Win>(addr).mv(); }
    static void cb_cd(Address, Address addr) { reference_to<Win>(addr).cd(); }
    static void cb_ad(Address, Address addr) { reference_to<Win>(addr).ad(); }
    
    static void cb_quit(Address, Address addr) { reference_to<Win>(addr).quit(); }
};

Lines_window::Lines_window(Point xy, int w, int h, const string& title)
    :   Window{xy,w,h,title},
        next_button{Point{x_max()-160,5}, 80, 20, "Next point", cb_next},
        quit_button{Point{x_max()-75,5}, 70, 20, "Quit", cb_quit},
        next_x{Point{x_max()- 330,5}, 50, 20, "next x:"},
        next_y{Point{x_max()-220,5}, 50, 20, "next y:"},
        xy_out{Point{100,5}, 100, 20, "current (x,y):"},
        color_menu{Point{x_max()-100,35},100,20,Menu::vertical,"Color"},
        menu_button{Point{x_max()-100,35},100,20,"Color Menu", cb_menu}
{
    attach(next_button);
    attach(quit_button);
    attach(next_x);
    attach(next_y);
    attach(xy_out);
    xy_out.put("no point");
    color_menu.attach(new Button{Point{0,0},0,0,"red",cb_red});
    color_menu.attach(new Button{Point{0,0},0,0,"blue",cb_blue});
    color_menu.attach(new Button{Point{0,0},0,0,"black",cb_black});
    attach(color_menu);
    color_menu.hide();
    attach(menu_button);
    attach(lines);
}

void Lines_window::quit()
{
    hide(); // curious FLTK idiom to delete window
}

void Lines_window::next()
{
    int x = next_x.get_int();
    int y = next_y.get_int();
    lines.add(Point{x,y});
    // update current position readout:
    ostringstream ss;
    ss << '(' << x << ',' << y << ')';
    xy_out.put(ss.str());
    redraw();
}


Win::Win(int w, int h, const string& t) 
    : Window{w,h,t}, sh_left{true}, mv_left{true}, cd_left{true}, ad_left{true}, score_text{Point{10,h-20},"Score = "+to_string(score)}
{
    p1 = new Button{Point{100,100},50,20,"show",cb_sh};
    p2 = new Button{Point{200,100},50,20, "hide",cb_sh};

    mvp = new Button{Point{100,200},50,20,"move",cb_mv};

    cdp = new Button{Point{100,300},50,20,"create",cb_cd};

    adp1 = new Button{Point{100,400},60,20,"activate",cb_ad};
    adp2 = new Button{Point{200,400},80,20,"deactivate",cb_ad};

    quit_button = new Button{Point{150,500},60,20,"QUIT",cb_quit};

    attach(*p1);
    attach(*p2);
    attach(*mvp);
    attach(*cdp);
    p2->hide(); // this one is hidden
    attach(*adp1);
    // adp2 is not attached

    attach(*quit_button);
    attach(score_text);
}

Win::~Win() { delete p1;delete p2;delete mvp;delete cdp;delete adp1;delete adp2;delete quit_button; } 
// This  was weirdly missing in the base implementation

// Action member functions
void Win::sh()
{
    if (sh_left) {
        p1->hide();
        p2->show();
    }
    else {
        p1->show();
        p2->hide();
    }

    sh_left = !sh_left;
    increment_score();
}

void Win::mv() // move the button
{
    if (mv_left) {
        mvp->move(100,0);
    }
    else {
        mvp->move(-100,0);
    }

    mv_left = !mv_left;
    increment_score();
}

void Win::cd() // delete the button and create a new one
{
    cdp->hide();
    delete cdp;
    string lab = "create";
    int x = 100;
    if (cd_left) {
        lab = "delete";
        x = 200;
    }
    
    cdp = new Button{Point{x,300}, 50, 20, lab, cb_cd};
    attach(*cdp);
    
    cd_left = !cd_left;
    increment_score();
}


void Win::ad() // detach the button from the window and attach its replacement
{
    if (ad_left) {
        detach(*adp1);
        attach(*adp2);
    }
    else {
        detach(*adp2);
        attach(*adp1);
    }
    
    ad_left = !ad_left;
    increment_score();
}

void Win::quit()
{
    hide(); // quit the window
}

void Win::increment_score()
{
    // detach(score_text);
    ++score;
    score_text.set_label("Score = "+to_string(score));
    // attach(score_text);
    redraw();
}

int main(int argc, char const *argv[])
{
    try
    {
        Win w {400,600,"Move!"};
        Lines_window lw {Point{900,100},620,400,"Lines!"};
        return gui_main();   
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }
    catch(...)
    {
        return 2;
    }
}

/*
    Compile with
    g++ -w -Wall ../PPP-graph-lib/Graph.cpp ../PPP-graph-lib/Window.cpp ../PPP-graph-lib/GUI.cpp main.cpp `fltk-config --ldflags --use-images`
*/ 