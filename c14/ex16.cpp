#include "../PPP-graph-lib/Simple_window.h"
#include "../PPP-graph-lib/Graph.h"

struct Controller
{
    virtual void on()=0;
    virtual void off()=0;
    virtual void set_level(int)=0;
    virtual void show()=0;
};

struct Simple_Controller : Controller
{
    void on() override { is_on = true; }
    void off() override { is_on = false;  }
    void set_level(int l) override { level = l; }
    void show() // not overridden just to show that it works even without hinting it 
    { 
        string on_off = (is_on)?"on":"off";
        std::cout << "This thing is " << on_off << " at level " << level << std::endl; 
    }

    private:
        bool is_on=false; // to avoid setting a constructor ..
        int level=0;
};

struct Shape_Color_Controller : Controller
{
    Shape_Color_Controller(Shape& ss) : s(ss) {}

    void on() override;
    void off() override;
    void set_level(int l) override;
    void show() // not overridden just to show that it works even without hinting it 
    { 
        string on_off = (s.color().visibility())?"visible":"invisible";
        std::cout << "This Shape is " << on_off << " with color-code " << s.color().as_int() << std::endl; 
    }

    private:
        Shape& s; //a Shape reference
};

void Shape_Color_Controller::on()
{
    s.set_color(Graph_lib::Color( Graph_lib::Color::Color_type(s.color().as_int()),Graph_lib::Color::Transparency::visible));
}

void Shape_Color_Controller::off()
{
    s.set_color(Graph_lib::Color( Graph_lib::Color::Color_type(s.color().as_int()),Graph_lib::Color::Transparency::invisible));
}

void Shape_Color_Controller::set_level(int cc)
{
    s.set_color(Graph_lib::Color{cc}); // set to visible
}




// helper
void print(Controller& c) { c.show(); }

int main(int argc, char const *argv[])
{
    try
    {
        Simple_Controller sc;
        print(sc);

        sc.set_level(3);
        sc.on();
        print(sc);

        Simple_window win{Point{200,200},800,600,"Visible Box"};

        Text_Box b{Point{150,150},400,300,0.2,"Much wow!"};
        
        // ---- 
        Shape_Color_Controller scc(b);
        scc.show();
        win.attach(b);
        win.wait_for_button();
        win.detach(b);

        scc.set_level(60); // should be dark green
        scc.show();
        
        win.set_label("Visible Dark-Green Box");
        win.attach(b);
        win.wait_for_button();
        win.detach(b);

        scc.off();
        scc.show();
        
        win.set_label("Invisible Box");
        win.attach(b);
        win.wait_for_button();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    return 0;
}

/*
    Compile with
    g++ -w -Wall ../PPP-graph-lib/Graph.cpp ../PPP-graph-lib/Window.cpp ../PPP-graph-lib/GUI.cpp ex16.cpp `fltk-config --ldflags --use-images`
*/ 