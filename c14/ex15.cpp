#include "../std_lib_facilities.h"

struct myIterator
{
    virtual double* next() =0;
};

struct Vector_Iterator : myIterator
{
    Vector_Iterator(vector<double>& vec) : v(vec) {}
    double* next() override 
    { 
        if(index<v.size())
        {
            ++index;
            return &(v[index-1]);
        }else
        {
            return nullptr;
        }
        
    }

    private:
        vector<double>::size_type index=0;
        vector<double>& v;
};

struct List_Iterator : myIterator
{
    List_Iterator(list<double>& list) : lst(list) {}
    double* next() override 
    { 
        if(index<lst.size())
        {
            ++index;
            list<double>::iterator it = lst.begin();
            for(int i=1; i<index; ++i) 
                ++it;   // this is needed to advance the std::_List_Iterator, which is kind of silly because we traverse the list each time.
                        // given that std::list has no random access though it seems the only way to me if we want to maintain
                        // an EXTERNAL iterator on it like the exercise asks..technically it works, so...

            return &(*it);
        }else
        {
            return nullptr;
        }
        
    }

    private:
        list<double>::size_type index=0;
        list<double>& lst;
};


void printIterator( myIterator& it )
{
    double* num = it.next();
    while( num )
    {
        std::cout << *num << " ";
        num = it.next();
    }
    std::cout << std::endl;
}

int main(int argc, char const *argv[])
{
    try
    {
        vector<double> vec {1,2,3,4,5,6,7};
        Vector_Iterator vit{vec};

        list<double> list {8,9,10,11,12,13,14};
        List_Iterator lit{list};


        printIterator(vit);
        printIterator(lit);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    return 0;
}