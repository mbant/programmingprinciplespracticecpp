#include "../PPP-graph-lib/Simple_window.h"
#include "../PPP-graph-lib/Graph.h"

int main ()
{
    try
    {
        using namespace Graph_lib;
        
        Point tl {150,150};
        // our graphics facilities are in Graph_lib
        // to become top left corner of window
        Simple_window win {tl,1200,800,"Canvas"};
        // screen coordinate tl for top left corner
        // window size(1200*800) 
        // title: Canvas

        // Create objects
        Arc a {Point{150,50}, 75,25, 45,270}; // starting from center-right-most point, anti-clockwise
        a.set_color(Color::dark_red);
        Mark m {a.center(),'x'};

        // Box b { Point{250,200}, 400, 200, 30};
        Box2 b2(Point{700,50}, 300, 150, 0.3);
        Text_Box tb(Point{150,150}, 300, 150, 0.3,"Test");
        Text_Box tb2(Point{350,350}, 300, 150, 0.5,"Test + Test");
        Text_Box tb3(Point{350,650}, 300, 90, 1,"Test + Test + Test");

        Arrow aa{ w(b2) , e(tb) };
        Arrow aa2{ s(tb) , nw(tb2) };
        Arrow aa3{ s(tb2) , n(tb3) };
        Arrow aa4{ s(b2) , ne(tb2) };
        Arrow aa5{ s(b2) , ne(tb3) };


        // Attach object to window
        win.attach(a);
        win.attach(m);

        // win.attach(b);
        win.attach(b2);
        win.attach(tb);
        win.attach(tb2);
        win.attach(tb3);
        win.attach(aa);
        win.attach(aa2);
        win.attach(aa3);
        win.attach(aa4);
        win.attach(aa5);

        win.wait_for_button(); // display!
    }
    catch(exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}