#include "../std_lib_facilities.h"

/*
Compute conversions between celsius and kelvin and back.
*/

double ctok(double c) // celsius to kelvin
{
    if( c < -273.15 ) error("There's nothing below absolute zero!"); // this essentially 'throw's a runtime_error()
    double k = c + 273.15;
    return k;
}

double ktoc(double k) // kelvin to celsius
{
    if( k < 0 ) error("There's nothing below absolute zero!");
    double c = k - 273.15;
    return c;
}


int main()
{
    
    double c = 0;
    try{
        cin >> c; // this doesn't really throw if no integer, damn!
    }
    catch(exception& e)
    {
        cerr << "error: " << e.what() ;
        cerr << "Please make sure to enter an number!\n";
        return 1;
    }

    try{
        double k = ctok(c);
        cout << k << '\n' ;
        return 0;
    }
    catch(exception& e)
    {
        cerr << "error: " << e.what() << '\n';
        // keep_window_open();
        return 1;
        // 1 indicates failure
    }
        catch (...) {
        cerr << "Oops: unknown exception!\n";
        // keep_window_open();
        return 2;
        // 2 indicates failure (of an unknown type)
    }
}