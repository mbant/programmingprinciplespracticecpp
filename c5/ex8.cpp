#include "../std_lib_facilities.h"

int main()
{

    cout << "Please enter the number of values you want to sum:\n";
    int n {0};
    cin >> n;

    cout << "Please enter some integers (press '|' to stop):\n";
    vector<int> v;
    double num{0};
    while( cin >> num )
    {
        try{
            v.push_back(narrow_cast<int>(num));    
        }
        catch( exception& e)
        {
            cerr << "Error: " << e.what() << "  -- remember you can only input integers!\n";
            return 1;
        }
    }

    if( n > v.size() )
    {
        error("You need to provide at least as many values as you asked me to sum!");
    }

    int sum {0};

    cout << "The sum of the first " << n << " numbers ( ";
    for( int i=0; i < n; ++i ){
        cout << v[i] << " ";
        sum += v[i];
    }
    cout << ") is " << sum << '\n';


    return 0;
}

// store as a double and check for narrowing conversions