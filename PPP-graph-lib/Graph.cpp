#include "Graph.h"
#include<map>

namespace Graph_lib {

void Shape::draw_lines() const
{
	if (color().visibility() && 1<points.size())	// draw sole pixel?
		for (unsigned int i=1; i<points.size(); ++i)
			fl_line(points[i-1].x,points[i-1].y,points[i].x,points[i].y);
}

void Shape::draw() const
{
	Fl_Color oldc = fl_color();
	// there is no good portable way of retrieving the current style
	fl_color(lcolor.as_int());
	fl_line_style(ls.style(),ls.width());
	draw_lines();
	fl_color(oldc);	// reset color (to pevious) and style (to default)
	fl_line_style(0);
}


// does two lines (p1,p2) and (p3,p4) intersect?
// if se return the distance of the intersect point as distances from p1
inline pair<double,double> line_intersect(Point p1, Point p2, Point p3, Point p4, bool& parallel) 
{
    double x1 = p1.x;
    double x2 = p2.x;
	double x3 = p3.x;
	double x4 = p4.x;
	double y1 = p1.y;
	double y2 = p2.y;
	double y3 = p3.y;
	double y4 = p4.y;

	double denom = ((y4 - y3)*(x2-x1) - (x4-x3)*(y2-y1));
	if (denom == 0){
		parallel= true;
		return pair<double,double>(0,0);
	}
	parallel = false;
	return pair<double,double>( ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3))/denom,
								((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3))/denom);
}


//intersection between two line segments
//Returns true if the two segments intersect,
//in which case intersection is set to the point of intersection
bool line_segment_intersect(Point p1, Point p2, Point p3, Point p4, Point& intersection){
   bool parallel;
   pair<double,double> u = line_intersect(p1,p2,p3,p4,parallel);
   if (parallel || u.first < 0 || u.first > 1 || u.second < 0 || u.second > 1) return false;
   intersection.x = p1.x + u.first*(p2.x - p1.x);
   intersection.y = p1.y + u.first*(p2.y - p1.y);
   return true;
} 

void Polygon::add(Point p)
{
	int np = number_of_points();

	if (1<np) {	// check that thenew line isn't parallel to the previous one
		if (p==point(np-1)) error("polygon point equal to previous point");
		bool parallel;
		line_intersect(point(np-1),p,point(np-2),point(np-1),parallel);
		if (parallel)
			error("two polygon points lie in a straight line");
	}

	for (int i = 1; i<np-1; ++i) {	// check that new segment doesn't interset and old point
		Point ignore(0,0);
		if (line_segment_intersect(point(np-1),p,point(i-1),point(i),ignore))
			error("intersect in polygon");
	}
	

	Closed_polyline::add(p);
}


void Polygon::draw_lines() const
{
		if (number_of_points() < 3) error("less than 3 points in a Polygon");
		Closed_polyline::draw_lines();
}

void Open_polyline::draw_lines() const
{
		if (fill_color().visibility()) {
			fl_color(fill_color().as_int());
			fl_begin_complex_polygon();
			for(int i=0; i<number_of_points(); ++i){
				fl_vertex(point(i).x, point(i).y);
			}
			fl_end_complex_polygon();
			fl_color(color().as_int());	// reset color
		}
		
		if (color().visibility())
			Shape::draw_lines();
}


void Closed_polyline::draw_lines() const
{
	Open_polyline::draw_lines();
		
	if (color().visibility())	// draw closing line:
		fl_line(point(number_of_points()-1).x,point(number_of_points()-1).y,point(0).x,point(0).y);
}
void Shape::move(int dx, int dy)
{
	for (unsigned int i = 0; i<points.size(); ++i) {
		points[i].x+=dx;
		points[i].y+=dy;
	}
}

void Lines::draw_lines() const
{
//	if (number_of_points()%2==1) error("odd number of points in set of lines");
	if (color().visibility())
		for (int i=1; i<number_of_points(); i+=2)
			fl_line(point(i-1).x,point(i-1).y,point(i).x,point(i).y);
}

void Text::draw_lines() const
{
	if(color().visibility())
	{
		int ofnt = fl_font();
		int osz = fl_size();
		fl_font(fnt.as_int(),fnt_sz);
		fl_draw(lab.c_str(), point(0).x, point(0).y);
		fl_font(ofnt,osz);
	}
}

Function::Function(Fct f, double r1, double r2, Point xy, int count, double xscale, double yscale)
// graph f(x) for x in [r1:r2) using count line segments with (0,0) displayed at xy
// x coordinates are scaled by xscale and y coordinates scaled by yscale
{
	if (r2-r1<=0) error("bad graphing range");
	if (count<=0) error("non-positive graphing count");
	double dist = (r2-r1)/count;
	double r = r1;
	for (int i = 0; i<count; ++i) {
		add(Point(xy.x+int(r*xscale),xy.y-int(f(r)*yscale)));
		r += dist;
	}
}

void Rectangle::draw_lines() const
{
	if (fill_color().visibility()) {	// fill
		fl_color(fill_color().as_int());
		fl_rectf(point(0).x,point(0).y,w,h);
		fl_color(color().as_int());	// reset color
	}

	if (color().visibility()) {	// edge on top of fill
		fl_color(color().as_int());
		fl_rect(point(0).x,point(0).y,w,h);
	}
}


Axis::Axis(Orientation d, Point xy, int length, int n, string lab)
	:label(Point(0,0),lab)
{
	if (length<0) error("bad axis length");
	switch (d){
	case Axis::x:
		{	Shape::add(xy);	// axis line
			Shape::add(Point(xy.x+length,xy.y));	// axis line
			if (1<n) {
				int dist = length/n;
				int x = xy.x+dist;
				for (int i = 0; i<n; ++i) {
					notches.add(Point(x,xy.y),Point(x,xy.y-5));
				x += dist;
			}
		}
		// label under the line
		label.move(length/3,xy.y+20);
		break;
	}
	case Axis::y:
		{	Shape::add(xy);	// a y-axis goes up
			Shape::add(Point(xy.x,xy.y-length));
			if (1<n) {
			int dist = length/n;
			int y = xy.y-dist;
			for (int i = 0; i<n; ++i) {
				notches.add(Point(xy.x,y),Point(xy.x+5,y));
				y -= dist;
			}
		}
		// label at top
		label.move(xy.x-10,xy.y-length-10);
		break;
	}
	case Axis::z:
		error("z axis not implemented");
	}
}

void Axis::draw_lines() const
{
	Shape::draw_lines();	// the line
	notches.draw();	// the notches may have a different color from the line
	label.draw();	// the label may have a different color from the line
}


void Axis::set_color(Color c)
{
	Shape::set_color(c);
	notches.set_color(c);
	label.set_color(c);
}

void Axis::move(int dx, int dy)
{
	Shape::move(dx,dy);
	notches.move(dx,dy);
	label.move(dx,dy);
}

void Circle::draw_lines() const
{
	if (fill_color().visibility()) {	// fill
		fl_color(fill_color().as_int());
		fl_pie(point(0).x,point(0).y,r+r-1,r+r-1,0,360);
		fl_color(color().as_int());	// reset color
	}

	if (color().visibility()) {
		fl_color(color().as_int());
		fl_arc(point(0).x,point(0).y,r+r,r+r,0,360);
	}
}


void Ellipse::draw_lines() const
{
	if (fill_color().visibility()) {	// fill
		fl_color(fill_color().as_int());
		fl_pie(point(0).x,point(0).y,w+w-1,h+h-1,0,360);
		fl_color(color().as_int());	// reset color
	}

	if (color().visibility()) {
		fl_color(color().as_int());
		fl_arc(point(0).x,point(0).y,w+w,h+h,0,360);
	}
}

void draw_mark(Point xy, char c)
{
	static const int dx = 4;
	static const int dy = 4;
	string m(1,c);
	fl_draw(m.c_str(),xy.x-dx,xy.y+dy);
}

void Marked_polyline::draw_lines() const
{
	Open_polyline::draw_lines();
	for (int i=0; i<number_of_points(); ++i) 
		draw_mark(point(i),mark[i%mark.size()]);
}
/*
void Marks::draw_lines() const
{
	for (int i=0; i<number_of_points(); ++i) 
		fl_draw(mark.c_str(),point(i).x-4,point(i).y+4);
}
*/


std::map<string,Suffix::Encoding> suffix_map;

int init_suffix_map()
{
	suffix_map["jpg"] = Suffix::jpg;
	suffix_map["JPG"] = Suffix::jpg;
	suffix_map["jpeg"] = Suffix::jpg;
	suffix_map["JPEG"] = Suffix::jpg;
	suffix_map["gif"] = Suffix::gif;
	suffix_map["GIF"] = Suffix::gif;
	suffix_map["bmp"] = Suffix::bmp;
	suffix_map["BMP"] = Suffix::bmp;
	return 0;
}

Suffix::Encoding get_encoding(const string& s)
		// try to deduce type from file name using a lookup table
{
	static int x = init_suffix_map();

	string::const_iterator p = find(s.begin(),s.end(),'.');
	if (p==s.end()) return Suffix::none;	// no suffix

	string suf(p+1,s.end());
	return suffix_map[suf];
}

bool can_open(const string& s)
            // check if a file named s exists and can be opened for reading
{
	ifstream ff(s.c_str());
	return static_cast<bool>(ff);
}


// somewhat overelaborate constructor
// because errors related to image files can be such a pain to debug
Image::Image(Point xy, string s, Suffix::Encoding e)
	:w(0), h(0), fn(xy,"")
{
	add(xy);

	if (!can_open(s)) {
		fn.set_label("cannot open \""+s+'\"');
		p = new Bad_image(30,20);	// the "error image"
		return;
	}

	if (e == Suffix::none) e = get_encoding(s);
	
	switch(e) {
	case Suffix::jpg:
		p = new Fl_JPEG_Image(s.c_str());
		break;
	case Suffix::gif:
		p = new Fl_GIF_Image(s.c_str());
		break;
//	case Suffix::bmp:
//		p = new Fl_BMP_Image(s.c_str());
//		break;
	default:	// Unsupported image encoding
		fn.set_label("unsupported file type \""+s+'\"');
		p = new Bad_image(30,20);	// the "error image"
	}
}

void Image::draw_lines() const
{
	if (fn.label()!="") fn.draw_lines();

	if (w&&h)
		p->draw(point(0).x,point(0).y,w,h,cx,cy);
	else
		p->draw(point(0).x,point(0).y);
}


// --------- c13

void Arc::draw_lines() const
{
	// no need to fill for a single Arc
	if (color().visibility())
		fl_arc(point(0).x,point(0).y,w+w,h+h,start,end);
}

bool validInputWH(int w, int h){
    if (w < 0 || h < 0) return false;
    else return true;
}

Box::Box( Point p, int ww, int hh, int ooff) // p is the top-left corner, off is the offset in the corners, how much of an arc to draw
: w(ww), h(hh), off(ooff)
{
	if(!validInputWH(w,h)) error("Not a valid box! Must provide non-negative width and height.");
	if(off>h/2 || off>w/2) error("Not a valid box! Must provide offset smaller than either side-length halved.");

	add(p);

	sides.add( Point{p.x+off,p.y} , Point{p.x+w-off,p.y} ); // top
	sides.add( Point{p.x,p.y+off} , Point{p.x,p.y+h-off} ); // left
	sides.add( Point{p.x+off,p.y+h} , Point{p.x+w-off,p.y+h} ); // bottom
	sides.add( Point{p.x+w,p.y+off} , Point{p.x+w,p.y+h-off} ); // right

	corners.push_back( new Arc{Point{p.x+w-off,p.y+off} ,off,off, 0,90} ); // tr
	corners.push_back( new Arc{Point{p.x+off,p.y+off} ,off,off, 90,180} ); // tl
	corners.push_back( new Arc{Point{p.x+off,p.y+h-off} ,off,off, 180,270} ); // bl
	corners.push_back( new Arc{Point{p.x+w-off,p.y+h-off} ,off,off, 270,360} ); // br
}

void Box::draw_lines() const
{
	if (color().visibility())
	{
		sides.draw_lines();
		for( int i=0; i<corners.size(); ++i )
			corners[i].draw_lines();
	}
}


void Box2::draw_lines() const
{
	if(color().visibility())
	{
		sides.draw_lines();
		
		for( int i=0; i<corners.size(); ++i )
			corners[i].draw_lines();
	}
}


Box2::Box2( Point p, int ww, int hh, float ooff) // p is the top-left corner, off is the offset in the corners, how much of an arc to draw
: w(ww), h(hh), off(ooff)
{
	if(!validInputWH(w,h)) error("Not a valid box! Must provide non-negative width and height.");
	if(off>1|off<0) error("Offset needs to be provided as a number between 0 (no offset) and 1 (offset = side-length/2)");
	if(off>h/2 || off>w/2) error("Not a valid box! Must provide offset smaller than either side-length halved.");

	add(p);

	const float x_off = 0.5*w*off;
	const float y_off = 0.5*h*off;

	sides.add( Point{p.x+x_off,p.y} , Point{p.x+w-x_off,p.y} ); // top
	sides.add( Point{p.x,p.y+y_off} , Point{p.x,p.y+h-y_off} ); // left
	sides.add( Point{p.x+x_off,p.y+h} , Point{p.x+w-x_off,p.y+h} ); // bottom
	sides.add( Point{p.x+w,p.y+y_off} , Point{p.x+w,p.y+h-y_off} ); // right

	corners.push_back( new Arc{Point{p.x+w-x_off,p.y+y_off} ,x_off,y_off, 0,90} ); // tr
	corners.push_back( new Arc{Point{p.x+x_off,p.y+y_off} ,x_off,y_off, 90,180} ); // tl
	corners.push_back( new Arc{Point{p.x+x_off,p.y+h-y_off} ,x_off,y_off, 180,270} ); // bl
	corners.push_back( new Arc{Point{p.x+w-x_off,p.y+h-y_off} ,x_off,y_off, 270,360} ); // br
}

Arrow::Arrow(Point p1, Point p2)
{
	Lines::add(p1,p2);

	// get the distance rho between them and scale the point as 10% of rho
	float rho = sqrt((p2.x-p1.x)*(p2.x-p1.x) + (p2.y-p1.y)*(p2.y-p1.y));
	int l = min(max(15,static_cast<int>(rho/10)),25);

	// get the angle between the two points and draw the two segments by rotating it of +-10'
	float phi = atan2(p1.y-p2.y,p1.x-p2.x);

	Lines::add(p2,Point{p2.x+l*cos(phi+0.3),p2.y+l*sin(phi+0.3)});
	Lines::add(p2,Point{p2.x+l*cos(phi-0.3),p2.y+l*sin(phi-0.3)});		
}

// -- 
Point nw(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x,tl.y};
}

Point n(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width()/2,tl.y};
}

Point ne(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width(),tl.y};
}

Point e(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width(),tl.y+r.height()/2};
}

Point se(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width(),tl.y+r.height()};
}

Point s(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width()/2,tl.y+r.height()};
}

Point sw(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x,tl.y+r.height()};
}

Point w(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x,tl.y+r.height()/2};
}

Point center(const Rectangle& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width()/2,tl.y+r.height()/2};
}

// Ex 5 for circles and ellipses is essentially the same as the functions above for the bounding box
// to get points on the shape itself instead we just need to solve the ellipse equation d(f1,{x,y})+d(f2,{x,y}) = c
// for x and y to get the composite one (n s e w are still on the bounding box)
// I'll demonstrate directly using the ellipses on the corner of the rounded boxes below

Point nw(const Box2& b)
{
	Point tl = b.point(0);

	// Get interesting quantities
	int ww = b.width();
	int hh = b.height();
	float oo = b.offset();
	float major = oo*ww;
	float minor = oo*hh;

	// Get the point on the bounding box and its ellipse center
	Point p{tl}; // already tl in this case
	Point c{p.x+0.5*major,p.y+0.5*minor};

	// Convert p in polar coordinates relative to the center
	float rho = sqrt((c.x-p.x)*(c.x-p.x) + (c.y-p.y)*(c.y-p.y));
	float phi = atan2(p.y-c.y,p.x-c.x);

	// Now the formula for an ellipse [centered at the origin] dictate that (x/major)^2 + (y/minor)^2 = 1 , so
	// so if this distance scaled is
	float rho_scaled = sqrt((c.x-p.x)*(c.x-p.x)/(major*major) + (c.y-p.y)*(c.y-p.y)/(minor*minor));
	// we can get the correct rho by dividing
	float r = rho*rho_scaled +1; //+1 cause I'd rather have the arrow on the outside..

	// get back in cartesian coordinate
	p.x = r*cos(phi);
	p.y = r*sin(phi);
	p.x += c.x;
	p.y += c.y;

	return p;
}

Point n(const Box2& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width()/2,tl.y};
}

Point ne(const Box2& b)
{
	Point tl = b.point(0);

	// Get interesting quantities
	int ww = b.width();
	int hh = b.height();
	float oo = b.offset();
	float major = oo*ww;
	float minor = oo*hh;

	// Get the point on the bounding box and its ellipse center
	Point p{tl.x+ww,tl.y};
	Point c{p.x-0.5*major,p.y+0.5*minor};

	// Convert p in polar coordinates relative to the center
	float rho = sqrt((c.x-p.x)*(c.x-p.x) + (c.y-p.y)*(c.y-p.y));
	float phi = atan2(p.y-c.y,p.x-c.x);

	// Now the formula for an ellipse [centered at the origin] dictate that (x/major)^2 + (y/minor)^2 = 1 , so
	// so if this distance scaled is
	float rho_scaled = sqrt((c.x-p.x)*(c.x-p.x)/(major*major) + (c.y-p.y)*(c.y-p.y)/(minor*minor));
	// we can get the correct rho by dividing
	float r = rho*rho_scaled +1; //+1 cause I'd rather have the arrow on the outside..

	// get back in cartesian coordinate
	p.x = c.x + r*cos(phi);
	p.y = c.y + r*sin(phi);

	return p;
}

Point e(const Box2& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width(),tl.y+r.height()/2};
}

Point se(const Box2& b)
{
	Point tl = b.point(0);

	// Get interesting quantities
	int ww = b.width();
	int hh = b.height();
	float oo = b.offset();
	float major = oo*ww;
	float minor = oo*hh;

	// Get the point on the bounding box and its ellipse center
	Point p{tl.x+b.width(),tl.y+b.height()};
	Point c{p.x-0.5*major,p.y-0.5*minor};

	// Convert p in polar coordinates relative to the center
	float rho = sqrt((c.x-p.x)*(c.x-p.x) + (c.y-p.y)*(c.y-p.y));
	float phi = atan2(p.y-c.y,p.x-c.x);

	// Now the formula for an ellipse [centered at the origin] dictate that (x/major)^2 + (y/minor)^2 = 1 , so
	// so if this distance scaled is
	float rho_scaled = sqrt((c.x-p.x)*(c.x-p.x)/(major*major) + (c.y-p.y)*(c.y-p.y)/(minor*minor));
	// we can get the correct rho by dividing
	float r = rho*rho_scaled +1; //+1 cause I'd rather have the arrow on the outside..

	// get back in cartesian coordinate
	p.x = r*cos(phi);
	p.y = r*sin(phi);
	p.x += c.x;
	p.y += c.y;

	return p;
}

Point s(const Box2& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width()/2,tl.y+r.height()};
}

Point sw(const Box2& b)
{
	Point tl = b.point(0);

	// Get interesting quantities
	int ww = b.width();
	int hh = b.height();
	float oo = b.offset();
	float major = oo*ww;
	float minor = oo*hh;

	// Get the point on the bounding box and its ellipse center
	Point p{tl.x,tl.y+b.height()};
	Point c{p.x+0.5*major,p.y-0.5*minor};

	// Convert p in polar coordinates relative to the center
	float rho = sqrt((c.x-p.x)*(c.x-p.x) + (c.y-p.y)*(c.y-p.y));
	float phi = atan2(p.y-c.y,p.x-c.x);

	// Now the formula for an ellipse [centered at the origin] dictate that (x/major)^2 + (y/minor)^2 = 1 , so
	// so if this distance scaled is
	float rho_scaled = sqrt((c.x-p.x)*(c.x-p.x)/(major*major) + (c.y-p.y)*(c.y-p.y)/(minor*minor));
	// we can get the correct rho by dividing
	float r = rho*rho_scaled +1; //+1 cause I'd rather have the arrow on the outside..

	// get back in cartesian coordinate
	p.x = r*cos(phi);
	p.y = r*sin(phi);
	p.x += c.x;
	p.y += c.y;

	return p;
}

Point w(const Box2& r)
{
	Point tl = r.point(0);
	return Point{tl.x,tl.y+r.height()/2};
}

Point center(const Box2& r)
{
	Point tl = r.point(0);
	return Point{tl.x+r.width()/2,tl.y+r.height()/2};
}

// ----------- c14

Shape::Shape(initializer_list<Point> lst)
{
	for( auto const& p : lst )
		add(p);
}



// Color Box2::color() const { return sides.color(); } // since the  color is the same for all elements...

/*
	This exercise exposed a limitation of Shape behaviour btw, as for composite Shapes (like Box or expecially Text_Box), if we want to color different
	sub-objects (like sides or text) differently, we get a conflict between 

	one solution is to implement the color changes and reset in the draw_lines() itself for each shape, like
	void Lines::draw_lines() const
	{
	//	if (number_of_points()%2==1) error("odd number of points in set of lines");
		if (color().visibility())
		{
			// fl_color(color().as_int());
			for (int i=1; i<number_of_points(); i+=2)
				fl_line(point(i-1).x,point(i-1).y,point(i).x,point(i).y);
			// fl_color(); // reset color
		}
	}
	(but that defies the purpose of Shape::draw() a bit) and also specify a set_color/color member for Text_Box as in
	void Box2::set_color(Color col) 
	{ 
		sides.set_color(col); 

		for(int i=0; i<corners.size(); ++i)
			corners[i].set_color(col);

	}
	for example ( to color only the box itself and not the text )....


	or we could have each object have a single color for all its elements as I did here, leaving intact the rest of the code
		This is of course a conceptual simplyfication we're making, but leaves the code more readable without changing the structure provided

	This actually messes up color visibility a bit as well if not careful, since when setting the color of a composite shape and drawing it we need to check
	globally for visilibity because each subelement (Shape themseves) might be visible as it's the default
 */


} // Graph