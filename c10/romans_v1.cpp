#include "../std_lib_facilities.h"

// 2) This version checks correctly for some error, like CMLIX is not valid anymore
// but the logic is flawed/incomplete -- IIX works the same as IXI and both say 10, while of course they're invalid!

// 3) Of course adapting the calculator to work with this is trivial, as we only need to generalise the Calculator::get_num() to read a Roman_int rather than a numeric double!
// and need the reverse function to get from an int to a Roman_int

// 4) This would be SO MUCH easier if we just had a string, cause we could simply match patterns on the string withouth going one char at a time, knowing how long it is,
// but this is an exercise on IO streams, so ...

// Also, I don't think the solution that uses subtraction is any worth it here, because of point 2)

/*
Roman_int

Roman numbers follow the following grammar

Allowed symbols:    I   V   X   L   C   D   M
                    1   5   10  50  100 500 1000

Grammar:

Number:

    Symbol

    Symbol Number

    I V Number
    I X Number
    X L Number
    X C Number
    C D Number
    C M Number

Symbol:
    any allowed symbol

*/

class Roman_int
{
    public:

        Roman_int( ) { roman_repr = ""; num = 0; }
        Roman_int( string& s );
        
        inline string as_roman() const { return roman_repr; }
        inline int as_int() const { return num; }
    
        bool is_valid();

        int num;
        string roman_repr;
};

bool Roman_int::is_valid()
{
    // static vector<string> valid_tokens = {"I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"};

    // Assume the roman has been read successfully, meaning is made only of valid characters
    // iterate through the string and check that the read tokens are in accordance with the valid_tokens ordering
    for ( size_t i=0; i<(roman_repr.size()-1); ++i ) //  I only need to check up to the second to last element
    {
        char c = roman_repr[i];
        char c2;
        switch (c)
        {
            case 'I':
                c2 = roman_repr[i+1]; // get the next char
                if ( c2 != 'I' && c2 != 'V' && c2 != 'X' )
                    return false;
                break;
        
            case 'V':
                c2 = roman_repr[i+1]; // get the next char
                if ( c2 != 'I' )
                    return false;
                break;
        
            case 'X':
                c2 = roman_repr[i+1]; // get the next char
                if ( c2 == 'D' || c2 == 'M' )
                    return false;
                break;
        
            case 'L':
                c2 = roman_repr[i+1]; // get the next char
                if ( c2 == 'C' || c2 == 'D' || c2 == 'M' )
                    return false;
                break;

            case 'D':
                c2 = roman_repr[i+1]; // get the next char
                if ( c2 == 'M' )
                    return false;
                break;

            case 'C': case 'M': default:
                // everything is possible after 'M' and 'C'
                break;
        }
    } 
    return true;
}

ostream& operator<<( ostream& os, Roman_int& r )
{
    return os << r.as_roman();
}

istream& operator>>( istream& is, Roman_int& r )
{
    int num = 0 ;
    char ch1;
    string roman = ""; // keep track of the roman_repr string

    while ( is >> std::noskipws >> ch1 ) // keep reading
    {
        if ( !is ) {
            is.unget();
            is.clear(ios_base::failbit);
            return is;
        }

        switch ( ch1 )
        {
            case 'M':
                roman += ch1;
                num += 1000;
                break;

            case 'D':
                roman += ch1;
                num += 500;
                break;
        
            case 'L':
                roman += ch1;
                num += 50;
                break;

            case 'V':
                num += 5;
                break;

            case 'C':
            {
                roman += ch1;
                // for C we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("something went wrong in the middle of a read");
                }

                switch (ch2)
                {
                    case 'D':
                        roman += ch2;
                        num += 400;
                        break;

                    case 'M':
                        roman += ch2;
                        num += 900;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 100;
                        break;
                }
                break;
            }
                
            case 'X':
            {
                roman += ch1;
                // for X we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("something went wrong in the middle of a read");
                }

                switch (ch2)
                {
                    case 'L':
                        roman += ch2;
                        num += 40;
                        break;

                    case 'C':
                        roman += ch2;
                        num += 90;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 10;
                        break;

                }
                break;
            }
                
            case 'I':
            {
                roman += ch1;
                // for I we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("something went wrong in the middle of a read");
                }

                switch (ch2)
                {
                    case 'V':
                        roman += ch2;
                        num += 4;
                        break;

                    case 'X':
                        roman += ch2;
                        num += 9;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 1;
                        break;

                }
                break;
            }
                
            default:
                is.unget();
                is.clear(ios_base::failbit); // if the next character is not a valid roman numeral, return a failbit
                
                r.num = num;
                r.roman_repr = roman;
                if( !r.is_valid() )
                    error("invalid roman numeral");

                return is;
        }

    }

    r.num = num;
    return is;
}

Roman_int::Roman_int( string& s )
{
    roman_repr = s;
    std::istringstream is(s);
    is >> *this;  // can I do this before the Roman is finished constructing?
}

int main()
{
    Roman_int r;

    cout << "Please insert a Roman number:\n";
    cin >> r;

    cout << "Roman " << r << " equals " << r.as_int() << '\n';
}