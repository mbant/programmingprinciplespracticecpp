#include "../std_lib_facilities.h"
// 1) Ok, refactor so that we can actually terminate with whitespace as well, not necessarily another character ( the while true is the culprit there! )
// 2) also, there are some extra rules to observe, this computes the correct number but doesn't enforce order!
// i.e. 959 = CMLIX , but if we write IXCML or IXLCM we still get 959, which is unfortunate !


// 3) Of course adapting the calculator to work with this is trivial, as we only need to generalise the Calculator::get_num() to read a Roman_int rather than a numeric double!
// and need the reverse function to get from an int to a Roman_int

// 4) This would be SO MUCH easier if we just had a string, cause we could simply match patterns on the string withouth going one char at a time, knowing how long it is,
// but this is an exercise on IO streams, so ...

// Also, I don't think the solution that uses subtraction is any worth it here, because of point 2)

/*
Roman_int

Roman numbers follow the following grammar

Allowed symbols:    I   V   X   L   C   D   M
                    1   5   10  50  100 500 1000

Grammar:

Number:

    Symbol

    Symbol Number

    I V Number
    I X Number
    X L Number
    X C Number
    C D Number
    C M Number

Symbol:
    any allowed symbol

*/

class Roman_int
{
    public:

        Roman_int( ) { roman_repr = ""; num = 0; }
        Roman_int( string& s );
        
        inline string as_roman() const { return roman_repr; }
        inline int as_int() const { return num; }
        int num;
    
    private:
        string roman_repr;
};

ostream& operator<<( ostream& os, Roman_int& r )
{
    return os << r.as_roman();
}

istream& operator>>( istream& is, Roman_int& r )
{
    int num = 0 ;
    while ( true ) // keep reading
    {
        char ch1;
        if ( !(is>>ch1) ) {
            is.unget();
            is.clear(ios_base::failbit);
            return is;
        }

        switch ( ch1 )
        {
            case 'M':
                num += 1000;
                break;

            case 'D':
                num += 500;
                break;
        
            case 'L':
                num += 50;
                break;

            case 'V':
                num += 5;
                break;

            case 'C':
            {
                // for C we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("Invalid roman numeral"); //something went wrong in the middle of a read
                }

                switch (ch2)
                {
                    case 'D':
                        num += 400;
                        break;

                    case 'M':
                        num += 900;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 100;
                        break;
                }
                break;
            }
                
            case 'X':
            {
                // for X we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("Invalid roman numeral"); //something went wrong in the middle of a read
                }

                switch (ch2)
                {
                    case 'L':
                        num += 40;
                        break;

                    case 'C':
                        num += 90;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 10;
                        break;

                }
                break;
            }
                
            case 'I':
            {
                // for I we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("Invalid roman numeral"); //something went wrong in the middle of a read
                }

                switch (ch2)
                {
                    case 'V':
                        num += 4;
                        break;

                    case 'X':
                        num += 9;
                        break;
                
                    case ';':
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 1;
                        break;

                }
                break;
            }
                
            default:
                is.unget();
                is.clear(ios_base::failbit); // if the next character is not a valid roman numeral, return a failbit
                r.num = num;
                return is;
        }

    }
}

Roman_int::Roman_int( string& s )
{
    roman_repr = s;
    std::istringstream is(s);
    is >> *this;  // can I do this before the Roman is finished constructing?
}

int main()
{
    Roman_int r;

    cout << "Please insert a Roman number,followed by a ';'\n";
    cin >> r;

    cout << "Roman" << r << " equals " << r.as_int() << '\n';
}