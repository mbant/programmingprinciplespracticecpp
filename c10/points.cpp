#include "../std_lib_facilities.h"

struct Point
{
    double x,y;
};

ostream& operator<<(ostream& os, Point p)
{
    return os << "(" << p.x << "," << p.y << ")";
}

istream& operator>>(istream& is, Point& p)
{
    char ch1;
    if ( !(is>>ch1) || ch1 != '(') {    // it's not a point if it doesn't start with (
        is.unget();
        is.clear(ios_base::failbit);
        return is;
    }

    char ch2;
    double x,y;
    is >> x;

    if (!(is>>ch2) || ch2!=',')  // messed-up reading, don't try to recover anymore
        error("bad reading");
    is >> y;

    char ch3;
    if (!(is>>ch3) || ch3!=')')  // messed-up reading
        error("bad reading");
    
    p.x = x;
    p.y = y;

    return is;
}

void end_of_in_loop(istream& ist, const string& message)
{
    if ( ist.eof() ) return;
    else if ( ist.fail() ) {
        ist.unget();
        ist.clear(ios_base::failbit);
        return;
    }else // must be bad
        error(message);
}

void end_of_out_loop(ostream& ost, const string& message)
{
    if ( ost.bad() )
        error(message);
    else if ( ost.fail() ) {
        // use term as terminator and/or separator
        ost.clear();
        if ( ost.good() ) return; // all is fine
        error(message);
    }
}

void fill_from_file( vector<Point>& points, string& name)
{
    ifstream ist {name};     // open file for reading
    ist.exceptions(ist.exceptions()|ios_base::badbit);
    if (!ist) error("can't open input file ",name);

    while ( true )
    {
        Point p;        // get a clean point each time around
        if(!(ist >> p)) break;
        points.push_back(p);
    }
    end_of_in_loop(ist,"bad end of file");
}

void out_to_file( vector<Point>& points, string& name)
{
    ofstream ost {name};     // open file for reading
    
    if (!ost) error("can't open input file ",name);

    for( auto& p : points )
    {
        if(!(ost << p)) break;
        ost << ' ';
    }
    end_of_out_loop(ost,"Something wrong in the output");
    //file getsclosed at the end of the loop
}

int main()
{
    cout << "Please insert four(4) 2D cartesian points with the format (x,y) \n";

    vector<Point> points;
    for( int i=0; i<4; ++i )
    {
        Point p;
        if( cin >> p )
            points.push_back(p);
        else
            error("bad input");        
    }

    cout << "\nHere are the points you prompted:\n";
    for ( auto& p : points )
        cout << p << "\t";


    cout << "\nI'm now saving them to a file named 'points.txt'\n";
    string file_name = "points.txt";
    out_to_file(points,file_name);
    cout << "\nDone!\n";


    cout << "\nFinally, I'll read the 'points.txt' file and prompt you with the points I have read, to double check\n";
    vector<Point> points2;
    fill_from_file(points2,file_name);

    for ( auto& p : points2 )
        cout << p << "\t";
    cout << endl;

    return 0;
}

// (0,2)   (2,2)   (8,9)   (2,2)