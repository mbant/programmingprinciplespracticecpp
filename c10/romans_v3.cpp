#include "../std_lib_facilities.h"

// 3) Of course adapting the calculator to work with this is "trivial" but tedious, as we only need to generalise the Calculator::get_num() to read a Roman_int rather than a numeric double!
// and need the reverse function to get from an int to a Roman_int; also 0 cannot be represented so ...

// 4) This would be SO MUCH easier if we just had a string, cause we could simply match patterns on the string withouth going one char at a time, knowing how long it is,
// but this is an exercise on IO streams, so ...

// Also, I don't think the solution that uses subtraction is worth it here, because of point 2)

class Roman_int
{
    public:

        Roman_int( ) { roman_repr = "I"; num = 1; } // zero cannot be represented in roman numerals
        Roman_int( string& s );
        
        inline string as_roman() const { return roman_repr; }
        inline int as_int() const { return num; }
    
        bool is_valid();

        int num;
        string roman_repr;
};


string get_next_token( string& roman , size_t& pos ) // would be better with an iterator
{
    if ( pos >= roman.size() )
        error("out of range");

    char c = roman[pos];
    char c2;
    switch (c)
        {
            case 'I':
                if ( pos == (roman.size()-1) )
                {
                    pos += 1;
                    return "I";
                }

                c2 = roman[pos+1]; // get the next char
                if ( c2 == 'X' )
                {
                    pos += 2;
                    return "IX";
                }else if ( c2 == 'V' )
                {
                    pos += 2;
                    return "IV";
                }

                pos += 1;
                return "I";
        
            case 'V':
                pos += 1;
                return "V";
        
            case 'X':
                if ( pos == (roman.size()-1) )
                {
                    pos += 1;
                    return "X";
                }

                c2 = roman[pos+1]; // get the next char
                if ( c2 == 'C' )
                {
                    pos += 2;
                    return "XC";
                }else if ( c2 == 'L' )
                {
                    pos += 2;
                    return "XL";
                }
                
                pos += 1;
                return "X";
        
            case 'L':
                pos += 1;
                return "L";

            case 'C':
                if ( pos == (roman.size()-1) )
                {
                    pos += 1;
                    return "C";
                }

                c2 = roman[pos+1]; // get the next char
                if ( c2 == 'M' )
                {
                    pos += 2;
                    return "CM";
                }else if ( c2 == 'D' )
                {
                    pos += 2;
                    return "CD";
                }
                
                pos += 1;
                return "C";

            case 'D':
                pos += 1;
                return "D";

            case 'M': 
                pos += 1;
                return "M";
            
            default:
                throw std::logic_error("Invalid roman numeral");
        }
}


/* Logic:

    token   |   can use after                    Val    order magn  log10    number of symbols
    I       |   I                                1      1           0        1
    IV      |   -                                4      1           0.       2
    V       |   I                                5      1           0.       1
    IX      |   -                                9      1           0.       2
    X       |   X,  IX, V, IV, I                 10     10          1        1
    XL      |       IX, V, IV, I                 40     10          1.       2
    L       |   X,  IX, V, IV, I                 50     10          1.       1
    XC      |       IX, V, IV, I                 90     10          1.       2
    C       |   C,  XC, L, XL, X, IX, V, IV, I   100    100         2        1
    CD      |       XC, L, XL, X, IX, V, IV, I   400    100         2.       2
    D       |   C,  XC, L, XL, X, IX, V, IV, I   500    100         2.       1
    CM      |       XC, L, XL, X, IX, V, IV, I   900    100         2.       2
    M       |   all                              1000   1000        3        1

So every token can be followed by another token having value of a LOWER order of magnitude than itself. 
Additionally, if the token has only one symbol, it can be followed by the first token in that order of magnitude.

NOTE: Scanning descendingly the valid_token map works as well, comparing with each token via std::equal.
But the requirements (iterators, std::algorithms, etc ...) are beyond the current stage of the book

--

Lasly any I,X,C or M token can be repeated a maximum of 3 times, every other token only once!
I,X,C and M are exactly the powers of 10, so we can just check if their log10 has decimals or not for example
of we could use

    static map<string,int> max_repeats = { {"I",2}, {"IV",0}, {"V",0}, {"IX",0}, {"X",2}, {"XL",0}, {"L",0}, {"XC",0}, {"C",2}, {"CD",0}, {"D",0}, {"CM",0}, {"M",2} };
    if ( count_repeated > max_repeats[current_token] )
        return false;
    to avoid several operations, but I liked the challenge once again :)        

*/
bool Roman_int::is_valid()
{
    static map<string,int> valid_tokens = { {"I",1}, {"IV",4}, {"V",5}, {"IX",9}, {"X",10}, {"XL",40}, {"L",50}, {"XC",90}, {"C",100}, {"CD",400}, {"D",500}, {"CM",900}, {"M",1000} };

    string previous_token = "", current_token="";
    int current_token_val = 0, max_acceptable_value = 1000, count_repeated;
    for ( size_t i=0; i < roman_repr.size(); ) // i is incremented by next-token
    {
        current_token = get_next_token( roman_repr , i );
        current_token_val = valid_tokens[ current_token ];

        // Check value
        if( current_token_val > max_acceptable_value )
            return false;

        // Check repeats
        if ( previous_token.compare(current_token) == 0 )
            ++count_repeated;
        else
            count_repeated =0;

        if ( count_repeated > ( fmod( log10( current_token_val ) , 1. ) == 0. )*2 )
            return false;
        
        // If all well, prepare the next step
        max_acceptable_value = std::pow( 10 , static_cast<int>( std::log10( current_token_val )) );
        if ( current_token.size() == 2 )
            max_acceptable_value -= 1;
        previous_token = current_token;

    }

    return true;

}

ostream& operator<<( ostream& os, Roman_int& r )
{
    return os << r.as_roman();
}


/* 
This next one would be uber-easy by just reading a string from isream and parse it (maybe via get_next_token?)
I wanna stick with chars for this exercise though
*/
istream& operator>>( istream& is, Roman_int& r )
{
    char ch1;
    int num = 0 ;
    string roman = ""; // keep track of the roman_repr string

    while ( is >> std::noskipws >> ch1 ) // keep reading
    {
        if ( !is ) {
            is.unget();
            is.clear(ios_base::failbit);
            return is;
        }

        switch ( ch1 )
        {
            case 'M':
                roman += ch1;
                num += 1000;
                break;

            case 'D':
                roman += ch1;
                num += 500;
                break;
        
            case 'L':
                roman += ch1;
                num += 50;
                break;

            case 'V':
                roman += ch1;
                num += 5;
                break;

            case 'C':
            {
                roman += ch1;
                // for C we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("something went wrong in the middle of a read");
                }

                switch (ch2)
                {
                    case 'D':
                        roman += ch2;
                        num += 400;
                        break;

                    case 'M':
                        roman += ch2;
                        num += 900;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 100;
                        break;
                }
                break;
            }
                
            case 'X':
            {
                roman += ch1;
                // for X we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("something went wrong in the middle of a read");
                }

                switch (ch2)
                {
                    case 'L':
                        roman += ch2;
                        num += 40;
                        break;

                    case 'C':
                        roman += ch2;
                        num += 90;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 10;
                        break;

                }
                break;
            }
                
            case 'I':
            {
                roman += ch1;
                // for I we need to look ahead
                char ch2;
                if ( !(is>>ch2) ) {
                    error("something went wrong in the middle of a read");
                }

                switch (ch2)
                {
                    case 'V':
                        roman += ch2;
                        num += 4;
                        break;

                    case 'X':
                        roman += ch2;
                        num += 9;
                        break;
                
                    default: // a valid char but something else, so unget and go on
                        is.unget();
                        num += 1;
                        break;

                }
                break;
            }
                
            default:
                is.unget();
                is.clear(ios_base::failbit); // if the next character is not a valid roman numeral, return a failbit
                
                if ( num == 0 )
                {
                    roman = "I";
                    num = 1;
                }

                r.num = num;
                r.roman_repr = roman;

                if( !r.is_valid() )
                    error("invalid roman numeral");

                return is;
        }

    }

    if ( num == 0 )
    {
        roman = "I";
        num = 1;
    }
    
    r.roman_repr = roman;
    r.num = num;

    if( !r.is_valid() )
        error("invalid roman numeral");

    return is;
}

Roman_int::Roman_int( string& s )
{
    roman_repr = s;
    std::istringstream is(s);
    is >> *this;  // can I do this before the Roman is finished constructing?
}

int main()
{
    Roman_int r;

    cout << "Please insert a Roman number:\n";
    cin >> r;

    cout << "Roman " << r << " equals " << r.as_int() << '\n';
}