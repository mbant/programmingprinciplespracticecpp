#include "../std_lib_facilities.h"

// Now that we have is_valid actally, we don't need a complex input method from istream. We get all (valid) symbols first and then check validity, easy!

// 3) Of course adapting the calculator to work with this is "trivial" but tedious, as we only need to generalise the Calculator::get_num() to read a Roman_int rather than a numeric double!
// and need the reverse function to get from an int to a Roman_int; also 0 cannot be represented so ...

// 4) This would be SO MUCH easier if we just had a string, cause we could simply match patterns on the string withouth going one char at a time, knowing how long it is,
// but this is an exercise on IO streams, so ...

// Also, I don't think the solution that uses subtraction is worth it here, because of point 2)

namespace Roman{

    const map<string,int> valid_tokens = { {"I",1}, {"IV",4}, {"V",5}, {"IX",9}, {"X",10}, {"XL",40}, {"L",50}, {"XC",90}, {"C",100}, {"CD",400}, {"D",500}, {"CM",900}, {"M",1000} };

    class Roman_int
    {
        public:

            Roman_int( ) { roman_repr = "I"; num = 1; } // zero cannot be represented in roman numerals
            Roman_int( string& s );
            Roman_int( int num_ );
            
            inline string as_roman() const { return roman_repr; }
            inline int as_int() const { return num; }
        
            static bool is_valid( string );
            static int get_int_from_roman( string );
            static string get_roman_from_int( int );

            inline bool is_valid() { return Roman_int::is_valid( roman_repr ); }

            int num;
            string roman_repr;
    };


    string get_next_token( string& roman , size_t& pos ) // would be better with an iterator
    {
        if ( pos >= roman.size() )
            error("out of range");

        char c = roman[pos];
        char c2;
        switch (c)
            {
                case 'I':
                    if ( pos == (roman.size()-1) )
                    {
                        pos += 1;
                        return "I";
                    }

                    c2 = roman[pos+1]; // get the next char
                    if ( c2 == 'X' )
                    {
                        pos += 2;
                        return "IX";
                    }else if ( c2 == 'V' )
                    {
                        pos += 2;
                        return "IV";
                    }

                    pos += 1;
                    return "I";
            
                case 'V':
                    pos += 1;
                    return "V";
            
                case 'X':
                    if ( pos == (roman.size()-1) )
                    {
                        pos += 1;
                        return "X";
                    }

                    c2 = roman[pos+1]; // get the next char
                    if ( c2 == 'C' )
                    {
                        pos += 2;
                        return "XC";
                    }else if ( c2 == 'L' )
                    {
                        pos += 2;
                        return "XL";
                    }
                    
                    pos += 1;
                    return "X";
            
                case 'L':
                    pos += 1;
                    return "L";

                case 'C':
                    if ( pos == (roman.size()-1) )
                    {
                        pos += 1;
                        return "C";
                    }

                    c2 = roman[pos+1]; // get the next char
                    if ( c2 == 'M' )
                    {
                        pos += 2;
                        return "CM";
                    }else if ( c2 == 'D' )
                    {
                        pos += 2;
                        return "CD";
                    }
                    
                    pos += 1;
                    return "C";

                case 'D':
                    pos += 1;
                    return "D";

                case 'M': 
                    pos += 1;
                    return "M";
                
                default:
                    throw std::logic_error("Invalid roman numeral");
            }
    }


    /* Logic:

        token   |   can use after                    Val    order magn  log10    number of symbols
        I       |   I                                1      1           0        1
        IV      |   -                                4      1           0.       2
        V       |   I                                5      1           0.       1
        IX      |   -                                9      1           0.       2
        X       |   X,  IX, V, IV, I                 10     10          1        1
        XL      |       IX, V, IV, I                 40     10          1.       2
        L       |   X,  IX, V, IV, I                 50     10          1.       1
        XC      |       IX, V, IV, I                 90     10          1.       2
        C       |   C,  XC, L, XL, X, IX, V, IV, I   100    100         2        1
        CD      |       XC, L, XL, X, IX, V, IV, I   400    100         2.       2
        D       |   C,  XC, L, XL, X, IX, V, IV, I   500    100         2.       1
        CM      |       XC, L, XL, X, IX, V, IV, I   900    100         2.       2
        M       |   all                              1000   1000        3        1

    So every token can be followed by another token having value of a LOWER order of magnitude than itself. 
    Additionally, if the token has only one symbol, it can be followed by the first token in that order of magnitude.

    NOTE: Scanning descendingly the valid_token map works as well, comparing with each token via std::equal.
    But the requirements (iterators, std::algorithms, etc ...) are beyond the current stage of the book

    --

    Lasly any I,X,C or M token can be repeated a maximum of 3 times, every other token only once!
    I,X,C and M are exactly the powers of 10, so we can just check if their log10 has decimals or not for example
    of we could use

        const map<string,int> max_repeats = { {"I",2}, {"IV",0}, {"V",0}, {"IX",0}, {"X",2}, {"XL",0}, {"L",0}, {"XC",0}, {"C",2}, {"CD",0}, {"D",0}, {"CM",0}, {"M",2} };
        if ( count_repeated > max_repeats.at( current_token ) )
            return false;
        to avoid several operations, but I liked the challenge once again :)        

    */
    bool Roman_int::is_valid( string r )
    {
        string previous_token = "", current_token="";
        int current_token_val = 0, max_acceptable_value = 1000, count_repeated;
        for ( size_t i=0; i < r.size(); ) // i is incremented by next-token
        {
            current_token = get_next_token( r , i );
            current_token_val = valid_tokens.at( current_token );

            // Check value
            if( current_token_val > max_acceptable_value )
                return false;

            // Check repeats
            if ( previous_token.compare(current_token) == 0 )
                ++count_repeated;
            else
                count_repeated =0;

            if ( count_repeated > ( fmod( log10( current_token_val ) , 1. ) == 0. )*2 )
                return false;
            
            // If all well, prepare the next step
            max_acceptable_value = std::pow( 10 , static_cast<int>( std::log10( current_token_val )) );
            if ( current_token.size() == 2 )
                max_acceptable_value -= 1;
            previous_token = current_token;
        }
        return true;
    }

    ostream& operator<<( ostream& os, Roman_int& r )
    {
        return os << r.as_roman();
    }

    /*
    This could clearly be implemented with (reverse) iterators, but, once again, this is only chapter 10
    */
    int symbol_value( const char c )
    {
        switch (c)
        {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                throw std::logic_error("Invalid roman symbol");          
        }
    }

    int Roman_int::get_int_from_roman( string r )
    {
        size_t n = r.size() -1 ;
        int previous = 0, current = 0, res = 0;

        for( int i=n; i >= 0 ; --i )
        {
            current = symbol_value( r[i] );

            if( current >= previous )
                res += current;
            else
                res -= current;

            previous = current;
        }
        return res;
    }


    enum class Symbol
    {
        one, five, ten
    };

    string Roman_int::get_roman_from_int( int num )
    {
        static const std::map<int, std::map<Symbol,char> > symbols {
            { 0 , { {Symbol::one, 'I'} , {Symbol::five , 'V'} , {Symbol::ten , 'X'} } } ,
            { 1 , { {Symbol::one, 'X'} , {Symbol::five , 'L'} , {Symbol::ten , 'C'} } } ,
            { 2 , { {Symbol::one, 'C'} , {Symbol::five , 'D'} , {Symbol::ten , 'M'} } } ,
            { 3 , { {Symbol::one, 'M'} } }
        };

        string roman = "";

        // Starting from the max order 3 ( thousands ), translate the integer into symbols using the above table
        // units of this order of magnitude to convert in symbols
        for ( int order=3; order>=0; --order )
        {
            int units = static_cast<int>( num / pow( 10 , order ) );
            switch (units)
            {
                case 0:
                    break; 

                case 1: case 2: case 3:
                    for ( unsigned int i=0; i<units; ++i)
                        roman += symbols.at(order).at(Symbol::one);
                    break;

                case 4:
                    roman += symbols.at(order).at(Symbol::one);  // note no break, intentional

                case 5:    
                    roman += symbols.at(order).at(Symbol::five);
                    break;

                case 6: case 7: case 8:
                    roman += symbols.at(order).at(Symbol::five);
                    for ( unsigned int i=0; i<(units-5); ++i)
                        roman += symbols.at(order).at(Symbol::one);
                    break;

                case 9:
                    roman += symbols.at(order).at(Symbol::one);  // note no break, intentional

                case 10:    // this should actually never occur ... maybe just merge with 9?
                    roman += symbols.at(order).at(Symbol::ten);
                    break;
            
                default:
                    throw std::logic_error("Invalid int to roman conversion");
            }
            // remainder, for lower orders
            num = fmod( num , pow( 10 , order ) );
        }

        return roman;
    }

}

using Roman::Roman_int;

istream& operator>>( istream& is, Roman_int& r )
{
    char ch1;
    string roman = ""; // keep track of the roman_repr string

    while ( is >> std::noskipws >> ch1 ) // keep reading
    {
        if ( !is ) {
            is.unget();
            is.clear(ios_base::failbit);
            return is;
        }

        switch ( ch1 )
        {
            case 'I': case 'V': case 'X': case 'L': case 'C': case 'D': case 'M':
                roman += ch1;
                break;
             
            default:
                is.unget();
                is.clear(ios_base::failbit); // if the next character is not a valid roman numeral, return a failbit
                break;
        }
    }

    if ( roman.empty() )
        roman = "I";

    if( Roman_int::is_valid( roman ) )
    {
        r.roman_repr = roman;
        r.num = Roman_int::get_int_from_roman( roman );
    }else
        error("invalid roman numeral");

    return is;
}

Roman_int operator+( Roman_int r1 , Roman_int r2 )
{
    // really easy add the integer representation and then deduce the roman repr
    return Roman_int( r1.as_int() + r2.as_int() );
}

Roman_int operator-( Roman_int r1 , Roman_int r2 )
{
    if ( r2.as_int() > r1.as_int() )
        throw std::logic_error("There's no such thing as a negative roman numeral you heretic!");
    return Roman_int( r1.as_int() - r2.as_int() );
}


Roman_int operator*( Roman_int r1 , Roman_int r2 )
{
    // no roman numeral can be negative OR zero, so this is safe
    return Roman_int( r1.as_int() * r2.as_int() );
}

Roman_int operator*( int i , Roman_int r )
{
    // no roman numeral can be negative OR zero, so this is safe
    return Roman_int( i * r.as_int() );
}

Roman_int operator/( Roman_int r1 , Roman_int r2 )
{
    if ( r2.as_int() > r1.as_int() )
        throw std::logic_error("You were just about to produce the first roman numeral representation of zero in more than 2000 years!");
    return Roman_int( r1.as_int() / r2.as_int() );
}

Roman_int::Roman_int( string& s )
{
    roman_repr = s;
    std::istringstream is(s);
    is >> *this;  // can I do this before the Roman is finished constructing? Just so I can the >> operator
}

Roman_int::Roman_int( int num_ )
{
    num = num_ ;
    roman_repr = get_roman_from_int ( num_ );
}

int main()
{
    cout << "Please insert a Roman number:  ";

    Roman_int r;
    cin >> r;

    cout << "Roman " << r << " equals " << r.as_int() << '\n';

    cout << "conversely " << r.as_int() << " equals " << Roman_int::get_roman_from_int( r.as_int() ) << '\n';

    Roman_int sum = r+r;
    Roman_int mult = 3*r;
    Roman_int diff = mult-r;
    Roman_int div = mult / r;
    cout << endl << sum << " -> "<< sum.as_int() << endl << mult<< " -> "<< mult.as_int() << 
            endl << diff<< " -> "<< diff.as_int() << endl << div<< " -> "<< div.as_int() << endl; 

}